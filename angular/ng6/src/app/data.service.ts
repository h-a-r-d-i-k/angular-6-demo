import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Headers, Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

	constructor(private http: HttpClient) { }
	
	getUsers(){
		return this.http.get('http://localhost/angular/users/userList');
	}
	getUser(id){
		return this.http.get('http://localhost/angular/users/userDetails/'+id);
	}
	getPosts(){
		return this.http.get('http://localhost/angular/posts');
	}
	saveUser(userData){
		return this.http.post('http://localhost/angular/users/saveUser',userData);
	}
}
