import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";


@Component({
  selector: 'add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.scss']
})
export class AddUsersComponent {

  log(variable){console.log(variable)}
  constructor(private data: DataService) { 
  }

  save(firstname, lastname, phone){
    var userdata = {
  		first_name : firstname,
  		last_name : lastname,
  		phone_primary : phone
  	}
  	this.data.saveUser(userdata).subscribe(
  		data => this.success = data.success
    );
  }
}
