$('document').ready(function () {
	$('#submit').click(function (argument) {
	
		signupDatasave();		
	});

$('#resend-regsiter-email').click(function (argument) {
	
		signupDatasave();		
	});


	function signupDatasave() {
		if ($('#agree_checked').is(":checked"))
		{
			var username = $('#username').val();
			var email = $('#email').val();
			var password = $('#password').val();
			var firstname = $('#firstname').val();
			var lastname = $('#lastname').val();
			var driver_licence_number = $('#driver_licence_number').val();
			var social_security_number = $('#social_security_number').val();
			var business_name = $('#business_name').val();
			var location = $('#location').val();
			var primary_phone = $('#primary_phone').val();
			var alt_phone = $('#alt_phone').val();
			var business_address = $('#business_address').val();
			var city = $('#city').val();
			var state = $('#state').val();
			var zip = $('#zip').val();
			var crew_members = $('#crew_members').val();
			var moving_miles = $('#moving_miles').val();
			var year_of_start_working = $('#year_of_start_working').val();
			var truck_information = $('#truck_information').val();
			var has_valid_business_license = $('#has_valid_business_license').val();
			var has_business_insurance = $('#has_business_insurance').val();
			var is_full_service_mover = $('#is_full_service_mover').val();
			var is_existing_service_provider = $('#is_existing_service_provider').val();
			var optional_message = $('#optional_message').val();

			if (truck_information == '') { has_truck = 0; } else { has_truck = 1; }

			var data = {
				username : username,
				email : email,
				password : password,
				first_name : firstname,
				last_name : lastname,
				driving_licence : driver_licence_number,
				social_security_number : social_security_number,
				business_name : business_name,
				address : location,
				phone_primary : primary_phone,
				phone_alternate : alt_phone,
				business_address : business_address,
				city : city,
				state : state,
				zip : zip,
				crew_member : crew_members,
				mile_for_job : moving_miles,
				starting_year : year_of_start_working,
				description_truck : truck_information,
				has_truck : has_truck,
				licensed_movers : has_valid_business_license,
				insurance : has_business_insurance,
				is_full_service_mover : is_full_service_mover,
				existing_mover_on_site : is_existing_service_provider,
				message_for_us : optional_message
			}
			console.log(data);

			$.ajax({
	                method: "POST",
	                url: './signupdata',
	                data: data,
	                success: function(data){
	                	data = JSON.parse(data);
	                	if (data['success'] == 1) {
	                		console.log(data);
	                		toastr.options.timeOut = 1500;
				            toastr.success('Record added successfully');
							detailingWebinar($id);
	                	} else {
				            toastr.options.timeOut = 1500;
				            toastr.error('Failed to add record');
	                	}
	                }
	            });

		} else {
			// error message for not ticked on agree checkbox
		}
	}


	$('#NextEmail').prop('disabled', true);
	$('#email').keyup(function() {
		if($('#email').val() != '') {
			data = { searchquery : $('#email').val(),field : 'email' }
			checking(data, function (response) {
				if (response == 1) {
					data = { searchquery : $('#username').val(),field : 'username' }
					checking(data, function (response) {
			   			if (response == 1) {
			   				console.log('disabled');
			   				$('#NextEmail').prop('disabled', false);
			   			} else {
			   				$('#NextEmail').prop('disabled',true);
			   			}
					})
				} else {
					$('#NextEmail').prop('disabled',true);
				}
			});
		} 
	});
	$('#username').keyup(function() {
		if($('#username').val() != '') {
			data = { searchquery : $('#username').val(),field : 'username' }
			checking(data, function (response) {
				if (response == 1) {
					data = { searchquery : $('#email').val(),field : 'email' }
					checking(data, function (response) {
			   			if (response == 1) {
			   				$('#NextEmail').prop('disabled', false);
			   			} else {
			   				$('#NextEmail').prop('disabled',true);
			   			}
					})
				} else {
					$('#NextEmail').prop('disabled',true);
				}
			});
		} 
	});
	function checking(data, callback) {
		$.ajax({
	        method: "POST",
	        url: './checkemail',
	        data: data,
	        success: callback
	    });
	}
});



function day_off() {
  $('.day-working .day-off').change(function () {
    if ($(this).prop('checked')) {
      $(this).parent().parent().find('.timepicker-work').prop('disabled', true);
    } else {
      $(this).parent().parent().find('.timepicker-work').prop('disabled', false);
    }

  })
}


// ========================================================
//            For Service provider profile  page 
// ========================================================

// For service areas cities
// ========================================================

function splittext() {
	var serviceAreaCites = $('#service-area-cities'),
	    serviceAreaCitesText = $('#service-area-cities').html();
	serviceAreaCites.html('<ul id="cities-menu"></ul>');

	var citiesArray = new Array();
	citiesArray = serviceAreaCitesText.split(",");
	for(var i = 0; citiesArray.length > i ; i++){
	  var city = "<li>"+citiesArray[i].replace(/ +(?= )/g,'')+"</li>";
	  $('#cities-menu').append(city);
	}
}


// function image_upload() {
// 	var image_profile_upload = '';
// 	console.log('image_upload');
// 	$('#image_picker_parent').on('change','#image-picker',function(){
// 		console.log('image changed');
// 		var file = this.files[0],
// 		  reader = new FileReader(),
// 		  img = $(this).siblings('img.picked-image')
// 		reader.onload = function (e) {
// 		  img.attr('src', e.target.result);
// 		  image_profile_upload = e.target.result;
// 		}
// 		reader.readAsDataURL(file);
// 	});

// 	// For checking file size
// 	var fileInput = $('#image-picker');
// 	var maxSize = fileInput.data('max-size');
// 	var maxSizeInBytes = maxSize * 1000000;

// 	// $('form').submit(function (e) {

// 	$('#image_upload').click(function () {

// 		if ($('form').find('input[type="file"]').length > 0) {

// 		  if (fileInput.get(0).files.length) {

// 		    var fileSize = fileInput.get(0).files[0].size; // in bytes
// 		    console.log(fileInput.get(0).files);
// 		    console.log(image_profile_upload);

// 		    if (fileSize > maxSizeInBytes) {

// 		      $('#image-picker+span').html('Your photo size is more than ' + maxSize + ' MB, please select another one');
// 		      return false;
// 		    } else {
// 		      var img = {
// 		            'img' : image_profile_upload
// 		        }
// 		      $.ajax({
// 		          url: "../Dashboard/ProfilePicUpload",
// 		          data: img, 
// 		          contentType: undefined,
// 		          type: 'post',
// 		          success : function(path){
// 		            // node.setAttribute('src', path);
// 		          }
// 		        });
// 		      $('#image-picker+span').html('');
// 		      console.log('hii');
// 		    }
// 		  }
// 		}

// 	});
// }