var setting = angular.module('setting',['toastr']);
setting.controller('accountsetting', function($scope, $http, $timeout, toastr) {

	function changeactive(link) {
		$scope.paidactive = '';
		$scope.passwordactive = '';
		$scope.emailactive = '';
		$scope.deleteactive = '';
		
		switch (link) {
            case 'paidactive':
				$scope.paidactive = "active";
                break;
            case 'deleteactive':
				$scope.deleteactive = "active";
            	break;
            case 'emailactive':
				$scope.emailactive = "active";
				break;
            case 'passwordactive':
				$scope.passwordactive = "active";
				break;
            default:
        }
	}

	//Uploading data format function
	var param = function (data) {
		var returnString = '';
		for (d in data) {
			if (data.hasOwnProperty(d))
				returnString += d + '=' + data[d] + '&';
		}
		// Remove last ampersand and return
		return returnString.slice(0, returnString.length - 1);
	}; //fxnend


	$scope.loading = false;
	$scope.password_update = function () {
		$scope.loading = true;
		$scope.tabShowfx = false;
		$scope.getTab = function () {
			return '../Dashboard/resetPassword';
		}//Loading the respective page
		
		$timeout(function(){
			$scope.loading = false;
			$scope.tabShowfx = true;
			changeactive('passwordactive');
		}, 300);
		
	}
	$scope.password_update();
	$scope.get_paid = function () {
		$scope.loading = true;
		$scope.tabShowfx = false;
		$scope.getTab = function () {
			return '../Dashboard/getPaid';
		}//Loading the respective page

		$timeout(function(){
			$scope.loading = false;
			$scope.tabShowfx = true;
			changeactive('paidactive');
		}, 300);
	}
	$scope.update_email = function () {
		$scope.loading = true;
		$scope.tabShowfx = false;
		$scope.getTab = function () {
			return '../Dashboard/updateEmail';
		}
		$timeout(function(){
			$scope.loading = false;
			$scope.tabShowfx = true;
			changeactive('emailactive');
		}, 300);
	}
	$scope.delete_account = function () {
		$scope.loading = true;
		$scope.tabShowfx = false;
		$scope.getTab = function () {
			return '../Dashboard/deleteAccountpage';
		}//Loading the respective page
		
		$timeout(function(){
			$scope.loading = false;
			$scope.tabShowfx = true;
			changeactive('deleteactive');
		}, 300);
	}

	$scope.changePassword = function(old_password,new_password,confirm_password) {
		$scope.loading = true;
		if (!old_password) {
			toastr.error("old password cannot be empty", "");
		} else if (!new_password) {
			toastr.error("new password cannot be empty", "");
		} else if (!confirm_password){
			toastr.error("confirm password cannot be empty", "");
		} else if (confirm_password != new_password){
			toastr.error("Confirmation password donot match", "");
		} else {
			data = {
				old_password : old_password,
				new_password : new_password,
				confirm_password : confirm_password
			}
			$http({
				method: 'POST',
				url: '../Dashboard/changePassword',
				data: param(data), // pass in data as strings
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				} // set the headers so angular passing info as form data (not request payload)
			}).then(function (response) {
				response = response.data;
				if (response.success == 0) {
					toastr.error(response.message, "");
				} else {
					toastr.success(response.message, "");
				}
				$('.changepassword').val('');				
			});
		}
		$scope.loading = false;
	}
	$scope.updateEmail = function ( new_email, password) {
		$scope.loading = true;
		if (!new_email) {
			toastr.error("Enter valid email address", "");
		} else if (!password) {
			toastr.error("Enter password", "");
		} else {
			data = {
				new_email : new_email,
				password : password,
			}
			$http({
				method: 'POST',
				url: '../Dashboard/changeEmail',
				data: param(data), // pass in data as strings
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				} // set the headers so angular passing info as form data (not request payload)
			}).then(function (response) {
				response = response.data;
				if (response.success == 0) {
					toastr.error(response.message, "");
				} else {
					toastr.success(response.message, "");
				}
				// $('.changepassword').val('');				
			});
		}
		$scope.loading = false;
	}
	$scope.deleteAccount = function (password) {
		$scope.loading = true;
		if (!password) {
			toastr.error("Enter password", "");
		} else {
			data = {
				password : password
			}
			$http({
				method: 'POST',
				url: '../Dashboard/deleteAccount',
				data: param(data), // pass in data as strings
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				} // set the headers so angular passing info as form data (not request payload)
			}).then(function (response) {
				response = response.data;
				if (response.success == 0) {
					toastr.error(response.message, "");
					$timeout($window.location.reload(), 3000);
				} else {
					toastr.success(response.message, "");
				}
				// $('.changepassword').val('');				
			});
		}
		$scope.loading = false;
	}
});