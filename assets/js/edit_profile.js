var providerEditApp = angular.module('providerEditApp',['toastr']).run(function($rootScope) {
  $rootScope.typeOf = function(value) {
    return typeof value;
  };
}).directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  };
});
providerEditApp.controller('serviceprovideredit', function($scope, $http, $timeout, toastr) {
	$scope.loading = false;
	function changeactive(link) {
		$scope.ProfilePicactive = '';
		$scope.Businessactive = '';
		$scope.Equipmentsactive = '';
		$scope.Serviceactive = '';
		$scope.Workingactive = '';
		$scope.Truckactive = '';
		$scope.Credentialsactive = '';
		$scope.Licensesactive = '';
		
		switch (link) {
            case 'ProfilePicactive':
				$scope.ProfilePicactive = "active";
                break;
            case 'Businessactive':
				$scope.Businessactive = "active";
            	break;
            case 'Equipmentsactive':
				$scope.Equipmentsactive = "active";
				break;
            case 'Serviceactive':
				$scope.Serviceactive = "active";
				break;
            case 'Workingactive':
				$scope.Workingactive = "active";
                break;
            case 'Truckactive':
				$scope.Truckactive = "active";
            	break;
            case 'Credentialsactive':
				$scope.Credentialsactive = "active";
				break;
            case 'Licensesactive':
				$scope.Licensesactive = "active";
				break;
            default:
        }
	}

	$scope.form = [];
	$scope.files = [];


	$scope.submit = function() {
		$scope.form.image = $scope.files[0];
		$http({
			method  : 'POST',
			url     : '../Dashboard/upload_image_angular',
			processData: false,
			transformRequest: function (data) {
				var formData = new FormData();
				formData.append("image", $scope.form.image);  
				return formData;  
			},  
			data : $scope.form,
			headers: {
			     'Content-Type': undefined
			}
		}).success(function(data){
			console.log(data);
			toastr.success("Update Successful", "");
		});
	};
	$scope.uploadedFile = function(element) {
		$scope.currentFile = element.files[0];
		var reader = new FileReader();
		reader.onload = function(event) {
			$scope.image_source = event.target.result
			$scope.$apply(function($scope) {
				$scope.files = element.files;
			});
		}
		reader.readAsDataURL(element.files[0]);
	}

	$scope.ProfilePic = function () {
		$scope.loading = true;
		$scope.tabShowfx = false;
		$scope.getTab = function () {
			return '../Dashboard/ProfilePic';
		}//Loading the respective page
		$http.get("../Dashboard/ProfilePicdata").then(function(response){
			response = response.data;
console.log(response);//<?php echo base_url(); ?>assets/uploads/image/
			if (response[0].profile_pic == '') {
				$scope.image_source = response.full_path+'user-img-2.png';
			} else {
				$scope.image_source = response.full_path+response[0].profile_pic;
			}
			changeactive('ProfilePicactive');
		}).catch(function (err) {
			$scope.loading = false;
			$scope.conn_error=true;
		}).finally(function () {
			// Hide loading spinner whether our call
			$timeout(function(){
				$scope.loading = false;
				$scope.tabShowfx = true;
			}, 300);
		});
	}

	$scope.ProfilePic();

	$scope.Business = function () {
		

		$scope.loading = true;
        $scope.tabShowfx = false;
		$scope.getTab = function () {
			return '../Dashboard/Business';
		}//Loading the respective page

		$scope.conn_error=false; //Error message
		// $scope.loading = true;  //Loading gif
		
		$http.get("../Dashboard/Businessdata").then(function(response){
			response = response.data;
			console.log(response);
			$scope.address = response[0].address;
			$scope.business_name = response[0].business_name;
			$scope.city = response[0].city;
			$scope.crew_member = response[0].crew_member;
			$scope.mile_for_job = response[0].mile_for_job;
			$scope.phone_alternate = response[0].phone_alternate;
			$scope.phone_primary = response[0].phone_primary;
			$scope.state = response[0].state;
			$scope.zip = response[0].zip;
			$scope.service_bio = response[0].service_bio;

			changeactive('Businessactive');
		}).catch(function (err) {
			$scope.loading = false;
			$scope.conn_error=true;
		}).finally(function () {
			// Hide loading spinner whether our call
			$timeout(function(){
				$scope.loading = false;
				$scope.tabShowfx = true;
			}, 300);
		});
	}

	$scope.SaveBusiness = function (address,business_name,city,crew_member,mile_for_job,phone_alternate,phone_primary,state,zip,service_bio) {
		console.log(service_bio);
		if (!address) {
			toastr.error("address required", "");
		} else if (!business_name) {
			toastr.error("Business Name required", "");
		} else if (!city) {
			toastr.error("city required", "");
		} else if (!crew_member) {
			toastr.error("crew_member required", "");
		} else if (!mile_for_job) {
			toastr.error("mile for job required", "");
		} else if (!phone_alternate) {
			toastr.error("alternate phone number required", "");
		} else if (!phone_primary) {
			toastr.error("primary phone number required", "");
		} else if (!state) {
			toastr.error("state required", "");
		} else if (!zip) {
			toastr.error("zipcode required", "");
		} else if (!service_bio) {
			toastr.error("Business Bio required", "");
		} else {
			$scope.loading = true;
			var data = 'address='+ address +
				'&business_name='+ business_name +
				'&city='+ city +
				'&crew_member='+ crew_member +
				'&mile_for_job='+ mile_for_job +
				'&phone_alternate='+ phone_alternate +
				'&phone_primary='+ phone_primary +
				'&state='+ state +
				'&zip='+ zip +
				'&service_bio='+ service_bio;
			$http({
				method: 'POST',
				url: "../Dashboard/update",
				data: data, // pass in data as strings
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				} // set the headers so angular passing info as form data (not request payload)
			}).then(function (response) {
				response = response.data;
				console.log(response);
				// if (response.success == 1) {
					toastr.success(response.message, "");
				// } else {
				// 	toastr.error(response.message, "");
				// }
				$scope.loading = false;
				//success response part not working
			});
		}
	}

	$scope.Service = function () {
		$scope.loading = true;
		$scope.tabShowfx = false;
		$scope.getTab = function () {
			return '../Dashboard/Service';
		}//Loading the respective page
		$http.get("../Dashboard/serviceData").then(function(response){
			response = response.data;
			if (!response) {
				$scope.manual_location = response[0].manual_location;
				$scope.currentLocation = response[0].geo_location;
			} else {
				$scope.manual_location = '';
				$scope.currentLocation = '';
			}
			$scope.getLocation();
			changeactive('Serviceactive')
		}).catch(function (err) {
			$scope.loading = false;
			$scope.conn_error=true;
		}).finally(function () {
			// Hide loading spinner whether our call
			$timeout(function(){
				$scope.loading = false;
				$scope.tabShowfx = true;
			}, 300);
		});
	}
	$scope.Working = function () {
		$scope.loading = true;
		$scope.tabShowfx = false;
		$scope.getTab = function () {
			return '../Dashboard/Working';
		}//Loading the respective page
		$http.get("../Dashboard/workingData").then(function(response){
			response = response.data;
			$scope.works = response; 
			$scope.worked = response;
			changeactive('Workingactive');
		}).catch(function (err) {
			$scope.loading = false;
			$scope.conn_error=true;
		}).finally(function () {
			// Hide loading spinner whether our call
			$timeout(function(){
				$scope.loading = false;
				timepicker_work();
				day_off();
				$scope.tabShowfx = true;
				angular.forEach($scope.worked, function(value , key) {
					$('#from_'+value.day_id).val(value.hours_from);
					$('#to_'+value.day_id).val(value.hours_to);
					if (value.is_off == 1 || value.is_off == "1") {
						$('#to_'+value.day_id).attr('disabled','true');
						$('#from_'+value.day_id).attr('disabled','true');
					}
				});
			}, 300);
		});
	}


	$scope.SaveWorking = function (w) {
		$scope.loading = true;
		data = $scope.worked;
		// console.log($scope.worked);
		console.log(data);

		angular.forEach(data, function(value,key) {
			console.log(value);
			var paramters =  {
				day_id: value.day_id,
				hours_from: $('#from_'+value.day_id).val(),
				hours_to: $('#to_'+value.day_id).val(),
				is_off: value.is_off
			} 
			$http({
				method: 'POST',
				url: "../Dashboard/workingHoursupdate",
				data: param(paramters), // pass in data as strings
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				} // set the headers so angular passing info as form data (not request payload)
			}).then(function (response) {
				// if (response == 1) {
				// } else {
				// 	toastr.error("Update Successful", "");
				// }
				//success response part not working
			});
		});
		toastr.success("Update Successful", "");
		$scope.loading = false;
	}

	//Uploading data format function
	var param = function (data) {
		var returnString = '';
		for (d in data) {
			if (data.hasOwnProperty(d))
				returnString += d + '=' + data[d] + '&';
		}
		// Remove last ampersand and return
		return returnString.slice(0, returnString.length - 1);
	}; //fxnend

	$scope.Truck = function () {
		$scope.loading = true;
		$scope.tabShowfx = false;
		$scope.getTab = function () {
			return '../Dashboard/Truck';
		}//Loading the respective page
		$http.get("../Dashboard/Truckdata").then(function(response){
			response = response.data[0];
			$scope.has_truck = response.has_truck;
			if ($scope.has_truck == "1") {
				$scope.truck_detail_style = { "display":"block" }
			} else {
				$scope.truck_detail_style = { "display":"none" }
			}
			$scope.description_truck = response.description_truck;
			changeactive('Truckactive');
		}).catch(function (err) {
			$scope.loading = false;
			$scope.conn_error=true;
		}).finally(function () {
			// Hide loading spinner whether our call
			$timeout(function(){
				$scope.loading = false;
				$scope.tabShowfx = true;
			}, 300);
		});
	}

	$scope.SaveTruck = function (has_truck, description_truck) {
		if (has_truck == 1 && !description_truck) {
			toastr.error('Truck description cannot be empty');
		} else {
			$scope.loading = true;
			var data = {
				has_truck : has_truck,
				description_truck : description_truck
			}
			console.log(data);
			$http({
				method: 'POST',
				url: "../Dashboard/update",
				data: param(data), // pass in data as strings
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				} // set the headers so angular passing info as form data (not request payload)
			}).then(function (response) {
				// if (response == 1) {
					toastr.success("Update Successful", "");
				// } else {
				// 	toastr.error("Update Successful", "");
				// }
				$scope.loading = false;
				//success response part not working
			});
		}
	}
	$scope.Licenses = function () {
		$scope.loading = true;
		$scope.tabShowfx = false;
		$scope.getTab = function () {
			return '../Dashboard/Licenses';
		}//Loading the respective page
		$http.get("../Dashboard/Licensesdata").then(function(response){
			response = response.data[0];
			$scope.licensed_movers = response.licensed_movers;
			$scope.insurance = response.insurance;
			$scope.is_full_service_mover = response.is_full_service_mover;
			$scope.business_license_number = response.business_license_number;
			$scope.business_policy_insurance_number = response.business_policy_insurance_number;

			if ($scope.licensed_movers == "1") {
				$scope.licensed_movers_style = { "display":"block" }
			} else {
				$scope.licensed_movers_style = { "display":"none" }
			}

			if ($scope.insurance == "1") {
				$scope.insurance_style = { "display":"block" }
			} else {
				$scope.insurance_style = { "display":"none" }
			}
			changeactive('Licensesactive');
		}).catch(function (err) {
			$scope.loading = false;
			$scope.conn_error=true;
		}).finally(function () {
			// Hide loading spinner whether our call
			$timeout(function(){
				$scope.loading = false;
				$scope.tabShowfx = true;
			}, 300);
		});
	}

	$scope.SaveLicense = function (licensed_movers,insurance,is_full_service_mover,business_license_number,business_policy_insurance_number) {
		if (licensed_movers == 1 && !business_license_number) {
			toastr.error("License Numebr cannot be empty", "");
		} else if (insurance == 1 && !business_policy_insurance_number ) {
			toastr.error("Policy insurance number cannot be empty", "");
		} else {
			$scope.loading = true;
			var data = {
				licensed_movers : licensed_movers,
				insurance : insurance,
				is_full_service_mover : is_full_service_mover,
				business_license_number : business_license_number,
				business_policy_insurance_number : business_policy_insurance_number
			}
			console.log(data);
			$http({
				method: 'POST',
				url: "../Dashboard/update",
				data: param(data), // pass in data as strings
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				} // set the headers so angular passing info as form data (not request payload)
			}).then(function (response) {
				// if (response == 1) {
					toastr.success("Update Successful", "");
				// } else {
				// 	toastr.error("Update Successful", "");
				// }
				$scope.loading = false;
				//success response part not working
			});
		}
	}

	$scope.licensed_movers_changed = function (licensed_movers) {
		console.log('licensed_movers');
		if (licensed_movers == "1") {
			$scope.licensed_movers_style = { "display":"block" }
		} else {
			$scope.licensed_movers_style = { "display":"none" }
		}
	}
	$scope.insurance_changed = function (insurance) {
		console.log('insurance');
		if (insurance == "1") {
			$scope.insurance_style = { "display":"block" }
		} else {
			$scope.insurance_style = { "display":"none" }
		}
	}
	$scope.truck_changed = function (has_truck) {
		if (has_truck == "1") {
			$scope.truck_detail_style = { "display":"block" }
		} else {
			$scope.truck_detail_style = { "display":"none" }
		}
	}


	$scope.Equipments = function () {
		$scope.loading = true;
		$scope.tabShowfx = false;
		$scope.blockShowfx = false;
		$scope.getTab = function () {
			return '../Dashboard/Equipments';
		}//Loading the respective page
		$http.get("../Dashboard/FetchEquipments").then(function(response){
			$scope.equipment_about = response.data.about[0].equipment_about;
			response = response.data.equipments;
			console.log(response);
			$scope.equipments = response;
			
			$scope.clickedEquipment = 0;
			changeactive('Equipmentsactive');
		}).catch(function (err) {
			$scope.loading = false;
			$scope.conn_error=true;
		}).finally(function () {
			// Hide loading spinner whether our call
			$timeout(function(){
				$scope.loading = false;
				$scope.tabShowfx = true;
			}, 300);
		});
		$scope.remove_new_equipment_style = {"display" : "none"}
	}

	$scope.add_new_equipment = function () {
		$scope.clickedEquipment = 1;
		console.log('s12');
		$scope.loading = true;
		$scope.equimentProcessType = 'addNew';
		$scope.blockShowfx = false;
		// $scope.getBlock = function () {
		// 	return '../Dashboard/NewEquipment';
		// }//Loading the respective page
		$scope.blockShowfx = true;
		$scope.is_included = 1;
		$scope.remove_new_equipment_style = {"display" : "inline-block"}
		$scope.flat_price_style = {"display" : "none"}
		$scope.loading = false;
	}
	$scope.remove_new_equipment = function () {
		$scope.clickedEquipment = 0;
		$scope.clickedCredential = 0;
		// $scope.getBlock = false;
		$scope.blockShowfx = false;
		$scope.remove_new_equipment_style = {"display" : "none"}
			clearscopevariableEquipment();
	}
	$scope.equipmentChoice = function (value) {
		$scope.is_included = value;
		if (value == 0) {
			$scope.flat_price_style = {"display" : "block"}
		} else {
			$scope.flat_price_style = {"display" : "none"}
		}
	}

	$scope.SaveEquipment = function (equipment_name,equipment_info,equipment_price){
		console.log(equipment_price);
		console.log(equipment_name);
		console.log(equipment_info);
		if (!equipment_name) {
			toastr.error("equipment name cannot be empty", "");
		} else if(!equipment_info) {
			toastr.error("equipment info cannot be empty", "");
		} else if(!equipment_price && $scope.is_included == 0) {
			toastr.error("equipment Price cannot be empty", "");
		} else {
			$scope.loading = true;
			if ($scope.is_included == 0) {
				var data = {
					item_name : equipment_name,
					item_description : equipment_info,
					is_included : $scope.is_included,
					price : equipment_price
				}
			} else {
				var data = {
					item_name : equipment_name,
					item_description : equipment_info,
					is_included : $scope.is_included
				}
			}
			if ($scope.equimentProcessType == 'addNew') {
				var url = "../Dashboard/AddNewEquipment";
			} else if ($scope.equimentProcessType == 'Update') {
				var url = "../Dashboard/updateEquipment";
				data.id = $scope.equip_id;
			}
			$http({
				method: 'POST',
				url: url,
				data: param(data), // pass in data as strings
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				} // set the headers so angular passing info as form data (not request payload)
			}).then(function (response) {
				response = response.data;
				// if (response.success == 1) {
					FetchEquipmentsDetails()
				// } else {
				// 	 toastr.error("Update Successful", "");
				// }
				$scope.clickedEquipment = 0;
				$scope.loading = false;
				// clearscopevariable();
			});
		}
	}
	function FetchEquipmentsDetails() {
		$http.get("../Dashboard/FetchEquipments").then(function(response){
			$scope.equipment_about = response.data.about[0].equipment_about;
			response = response.data.equipments;
			console.log(response);
			$scope.equipments = response;
			$scope.remove_new_equipment();
			if ($scope.equimentProcessType == 'addNew') {
				toastr.success("New record added", "");
			} else if ($scope.equimentProcessType == 'Update') {
				toastr.success("Record Updated Successful", "");
			}
		});
	}
	function clearscopevariableEquipment() {
		$scope.equip_id = '';
		$scope.is_included = '';
		$scope.equipment_name = '';
		$scope.equipment_info = '';
		$scope.equipment_price = '';
	}
	$scope.update_equipment = function (equip_id){
		$scope.loading = true;
		$scope.equimentProcessType = 'Update';
		$scope.blockShowfx = false;
		// $scope.getBlock = function () {
		// 	return '../Dashboard/NewEquipment';
		// }//Loading the respective page
		$http.get("../Dashboard/FetchEquipmentsEdit?id="+equip_id).then(function(response){
			response = response.data;

			$scope.equip_id = equip_id;
			$scope.is_included = response[0].is_included;
			$scope.equipment_name = response[0].item_name;
			$scope.equipment_info = response[0].item_description;
			$scope.equipment_price = response[0].price;

			if ($scope.is_included == 0) {
				$scope.flat_price_style = {"display" : "block"}
				$scope.price_included = false;
				$scope.price_included_not = true;
			} else {
				$scope.flat_price_style = {"display" : "none"}
				$scope.price_included = true;
				$scope.price_included_not = false;
			}

			$scope.blockShowfx = true;
			$scope.is_included = 1;
			$scope.remove_new_equipment_style = {"display" : "inline-block"}
			$scope.flat_price_style = {"display" : "none"}
			$scope.clickedEquipment = 1;
			
		}).catch(function (err) {
			$scope.loading = false;
			$scope.conn_error=true;
		}).finally(function () {
			// Hide loading spinner whether our call
			$timeout(function(){
				$scope.loading = false;
				$scope.tabShowfx = true;
			}, 300);
		});
	}

	$scope.delete_equipment = function (equip_id) {
		$scope.loading = true;
		$http({
			method: 'POST',
			url: '../Dashboard/DeleteEquipment',
			data: param({equip_id : equip_id}), // pass in data as strings
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			} // set the headers so angular passing info as form data (not request payload)
		}).then(function (response) {
			$scope.equipment_about = response.data.about[0].equipment_about;
			response = response.data.equipments;
			$scope.loading = false;
			toastr.success("Record deleted Successfully", "");
		});
	}

	$scope.Credentials = function () {
		$scope.loading = true;
		$scope.tabShowfx = false;
		$scope.getTab = function () {
			return '../Dashboard/Credentials';
		}//Loading the respective page
		$http.get("../Dashboard/FetchCredential").then(function(response){
			$scope.credential_about = response.data.about[0].credential_about;
			response = response.data.credentials;
			console.log(response);
			$scope.credentials = response;
			$scope.clickedCredential = 0;
			$scope.blockShowfx = false;
			changeactive('Credentialsactive');
		}).catch(function (err) {
			$scope.loading = false;
			$scope.conn_error=true;
		}).finally(function () {
			// Hide loading spinner whether our call
			$timeout(function(){
				$scope.loading = false;
				$scope.tabShowfx = true;
			}, 300);
		});
	}


	$scope.add_new_credential = function () {
		$scope.loading = true;
		$scope.credentialProcessType = 'addNew';
		$scope.blockShowfx = false;
		// $scope.getBlock = function () {
		// 	return '../Dashboard/NewCredential';
		// }//Loading the respective page
		$scope.blockShowfx = true;
		$scope.description_checkbox = 0;
		$scope.number_checkbox = 0;
		$scope.issuer_checkbox = 0;
		$scope.credential = {
				desc : '',
				number : '',
				issuer : ''
			}
		$scope.credential_name = '';
		console.log($scope.credential_name);
		$scope.remove_new_credential_style = {"display" : "inline-block"}
		$scope.clickedCredential = 1;
		$scope.loading = false;
	}
	$scope.updateCheckbox = function(checkboxvalue , checkboxname){
		console.log(checkboxvalue);
		switch (checkboxname) {
            case 'desc':
                if (checkboxvalue == 0) {
					$scope.credential_desc_style = {"display" : "block"}
				} else {
					$scope.credential_desc_style = {"display" : "none"}
				}
                break;
            case 'number':
                if (checkboxvalue == 0) {
					$scope.credential_number_style = {"display" : "block"}
				} else {
					$scope.credential_number_style = {"display" : "none"}
				}
				break;
            case 'issuer':
                if (checkboxvalue == 0) {
					$scope.credential_issuer_style = {"display" : "block"}
				} else {
					$scope.credential_issuer_style = {"display" : "none"}
				}
				break;
            default:
        }
    }
	$scope.SaveCredential = function(credential_name, credential, description_checkbox,number_checkbox,issuer_checkbox){
		
		if (!credential_name) {
			toastr.error("credential_name field cannot be empty", "");
		} else if (description_checkbox == 1 && !credential.desc) {
			toastr.error("credential_description field cannot be empty", "");
		} else if (number_checkbox == 1 && !credential.number) {
			toastr.error("credential_policy_number field cannot be empty", "");
		} else if (issuer_checkbox == 1 && !credential.issuer) {
			toastr.error("credential_policy_issuer field cannot be empty", "");
		} else {
			$scope.loading = true;
			var data = {
				name : credential_name,
				has_description : description_checkbox, 
				credential_description : credential.desc, 
				credential_policy_number : credential.number, 
				has_policy_date : number_checkbox, 
				has_policy_issuer : issuer_checkbox, 
				credential_policy_issuer : credential.issuer
			}

			if ($scope.credentialProcessType == 'addNew') {
				var url = "../Dashboard/SaveCredential";
			} else if ($scope.credentialProcessType == 'Update') {
				var url = "../Dashboard/updateCredential";
				data.id = $scope.credential_id;
			}
			console.log(data);
			$http({
				method: 'POST',
				url: url,
				data: param(data), // pass in data as strings
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				} // set the headers so angular passing info as form data (not request payload)
			}).then(function (response) {
				$scope.credential_about = response.data.about[0].credential_about;
				response = response.data.credentials;
				$scope.credentials = response;
				toastr.success("Record updated Successfully", "");
				
				// $scope.remove_new_equipment();
				// toastr.success("Update Successful", "");
				// } else {
				// 	 toastr.error("Update Successful", "");
				// }
				// clearscopevariable();
				$scope.clickedCredential = 0;
				$scope.loading = false;
				$scope.blockShowfx = false;
			});
		}
	}
	

	$scope.update_credential = function (credential_id){
		$scope.loading = true;
		$scope.credentialProcessType = 'Update';
		$scope.blockShowfx = false;
		// $scope.getBlock = function () {
		// 	return '../Dashboard/NewCredential';
		// }//Loading the respective page
		$http.get("../Dashboard/FetchCredentialEdit?id="+credential_id).then(function(response){
			response = response.data;
			console.log(response);

			$scope.credential_id = credential_id;

			$scope.credential_name = response[0].name;
			
			$scope.credential = {
				desc : response[0].credential_description,
				number : response[0].credential_policy_number,
				issuer : response[0].credential_policy_issuer
			}
			
			$scope.description_checkbox = response[0].has_description;
			$scope.number_checkbox = response[0].has_policy_date;
			$scope.issuer_checkbox = response[0].has_policy_issuer;


			if ($scope.description_checkbox == 1) {
				$scope.credential_desc_style = {"display" : "block"}
			} else {
				$scope.credential_desc_style = {"display" : "none"}
			}
			if ($scope.number_checkbox == 1) {
				$scope.credential_number_style = {"display" : "block"}
			} else {
				$scope.credential_number_style = {"display" : "none"}
			}
			if ($scope.issuer_checkbox == 1) {
				$scope.credential_issuer_style = {"display" : "block"}
			} else {
				$scope.credential_issuer_style = {"display" : "none"}
			}
			
			$scope.blockShowfx = true;
			$scope.is_included = 1;
			$scope.remove_new_equipment_style = {"display" : "inline-block"}
			$scope.flat_price_style = {"display" : "none"}
			
			$scope.clickedCredential = 1;
		}).catch(function (err) {
			$scope.loading = false;
			$scope.conn_error=true;
		}).finally(function () {
			// Hide loading spinner whether our call
			$timeout(function(){
				$scope.loading = false;
				$scope.tabShowfx = true;
			}, 300);
		});
	}

	$scope.delete_credential = function (credential_id) {
		$scope.loading = true;
		$http({
			method: 'POST',
			url: '../Dashboard/DeleteCredential',
			data: param({credential_id : credential_id}), // pass in data as strings
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			} // set the headers so angular passing info as form data (not request payload)
		}).then(function (response) {
			$scope.credential_about = response.data.about[0].credential_about;
			response = response.data.credentials;
			$scope.credentials = response;
			$scope.loading = false;
			toastr.success("Record deleted Successfully", "");
		});
	}

	$scope.savelatlong = function (latlng) {
		$scope.loading = true;
		data = {
			'geo_location' : $scope.currentLocation,
			'latitude' : latlng.latitude,
			'longitude' : latlng.longitude
		}
		console.log(data);
		$http({
			method: 'POST',
			url: '../Dashboard/savelatlong',
			data: param(data), // pass in data as strings
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			} // set the headers so angular passing info as form data (not request payload)
		}).then(function (response) {
			response = response.data;
			// $scope.credentials = response;
			$scope.loading = false;
		});
	}

	$scope.getLocation = function() {
	  if (navigator.geolocation) {
	      navigator.geolocation.getCurrentPosition($scope.showPosition);
	  } else {
	      console.log("Geolocation is not supported by this browser.");
	  }
	}
	$scope.showPosition = function(position) {
		$scope.loading = true;
		$scope.blockShowfx = false;
		var data = {
			'latitude' : position.coords.latitude,
			'longitude' : position.coords.longitude
		}
		$http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+data.latitude+','+data.longitude+'&key=AIzaSyA4dXiCdayvN1jBkLMK_07J7oRNqt9xg3Y').then(function(response){
		  	
			// $scope.currentLocation = '1111';
		  	$scope.currentLocation = response.data.plus_code.compound_code;

		  	console.log(response.data.plus_code.compound_code);
			response = response.data;
  			var map = '<iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q='+data.latitude+','+data.longitude+'&key=AIzaSyA4dXiCdayvN1jBkLMK_07J7oRNqt9xg3Y&zoom=10"></iframe>';
			document.getElementById('editmaps').innerHTML = map;

			$scope.savelatlong(data);
			$scope.blockShowfx = true;

		}).catch(function (err) {
			$scope.loading = false;
			$scope.conn_error=true;
		}).finally(function () {
			// Hide loading spinner whether our call
			$timeout(function(){
				$scope.loading = false;
				$scope.tabShowfx = true;
			}, 300);
		});
	  
  		console.log(data);
	}

	$scope.saveManual = function(manual_location){
		if (!manual_location) {
			toastr.error("Location field cannot be empty", "");
		} else {
			$scope.loading = true;
			data = {
				'manual_location' : manual_location
			}
			console.log(data);
			$http({
				method: 'POST',
				url: '../Dashboard/savelatlong',
				data: param(data), // pass in data as strings
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				} // set the headers so angular passing info as form data (not request payload)
			}).then(function (response) {
				response = response.data;
				toastr.success("Record updated Successfully", "");
				$scope.credentials = response;
				$scope.loading = false;
			});
		}
	}

	$scope.save_about = function (about, field) {
		if (!about) {
			toastr.error("About Message cannot be blank", "");
		} else {
			$scope.loading = true;
			data = {
				'field' : field,
				'about' : about
			}
			console.log(data);
			$http({
				method: 'POST',
				url: '../Dashboard/saveAbout',
				data: param(data), // pass in data as strings
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				} // set the headers so angular passing info as form data (not request payload)
			}).then(function (response) {
				response = response.data;
				// $scope.credentials = response;
				$scope.loading = false;
				toastr.success("Record updated Successfully", "");
			});
		}
	}
});


		