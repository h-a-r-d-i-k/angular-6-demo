<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Dashboard_model extends CI_Model{

	    public function retrive($user_id, $table, $fields, $join = array())
	    {
	    	$this->db->select($fields);
	    	$this->db->where('user_id',$user_id);

	    	if (!empty($join)) {
	    		foreach ($join as $value) {
		    		$this->db->join($value['ref'], $value['query'], $value['type']);
	    		}
	    	}

			$query=$this->db->get($table);
			
			return $query->result_array();

	    }

	    public function retriveEmail($email, $table, $fields, $join = array())
	    {
	    	$this->db->select($fields);
	    	$this->db->where('email',$email);

	    	if (!empty($join)) {
	    		foreach ($join as $value) {
		    		$this->db->join($value['ref'], $value['query'], $value['type']);
	    		}
	    	}

			$query=$this->db->get($table);
			return $query->result_array();

	    }

	    public function retriveEquipment($id, $table, $fields)
	    {
	    	$this->db->select($fields);
	    	$this->db->where('id',$id);

			$query=$this->db->get($table);
			
			return $query->result_array();

	    }

	    public function update($user_id, $table, $data)
	    {
	    	$this->db->where('user_id',$user_id);

			$this->db->update($table,$data);

			return $this->db->affected_rows();

	    }

	    public function workingHoursupdate($user_id, $table, $data)
	    {
	    	$day = $data['day_id'];

	    	$data = array('is_off' => $data['is_off'], 'hours_from' => $data['hours_from'], 'hours_to' => $data['hours_to']);
	    	$this->db->where('user_id',$user_id);
	    	$this->db->where('day_id',$day);

			$this->db->update($table,$data);

			return $this->db->affected_rows();

	    }

	    function insert($table_name,$data)
	    {
	        $this->db->insert($table_name,$data);
			return $this->db->affected_rows();
	    }

	    function savelatlong($user_id,$table_name,$data)
	    {
	        $this->db->where('user_id',$user_id);
			$q = $this->db->get($table_name);

			if ( $q->num_rows() > 0 ) 
			{
				unset($data['user_id']);
				$this->db->where('user_id',$user_id);
				$this->db->update($table_name,$data);
				return $this->db->affected_rows();
			} else {
				$this->db->insert($table_name,$data);
				return $this->db->affected_rows();
			}
	    }

	    function userAuthentication($email,$password)
		{
			$this->db->where('email',$email);
			$this->db->or_where('username',$email);
			$this->db->where('password',$password);
			$query = $this->db->get('tbl_users');
			return $query->result();

		}
	    public function updateEquipment($id, $table, $data)
	    {
	    	$this->db->where('id',$id);

			$this->db->update($table,$data);

			return $this->db->affected_rows();

	    }

	    public function DeleteEquipment($equip_id)
	    {
			$this->db->where('id', $equip_id);
			$this->db->delete('tbl_equipment'); 
	    }
	    public function DeleteCredential($credential_id)
	    {
			$this->db->where('id', $credential_id);
			$this->db->delete('tbl_credential'); 
	    }

	    public function checkpassword($user_id , $old_password)
	    {
	    	$this->db->where('user_id',$user_id);
	    	$this->db->where('password',md5($old_password));
			$this->db->where('active','1');
			
			$query=$this->db->get('tbl_users');

			if(!empty(json_decode(json_encode($query->result()) , true))){
				$response = 1;
			}else{
				$response = 0;
			}
			return $response;
	    }

	}