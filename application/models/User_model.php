<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class User_model extends CI_Model{

		public function retriveUserlist()
		{
			$query = $this->db->get('tbl_users');
			return $query->result_array();
		}

	    function insert($table_name,$data)
	    {
	        $this->db->insert($table_name,$data);
			return $this->db->affected_rows();
	    }
	    function retriveUserDetails($id)
	    {
	    	$this->db->where('user_id',$id);
			$query = $this->db->get('tbl_users');
			return $query->result_array();
	    }

	}