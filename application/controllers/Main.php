<?php
	/**
	 * Controller Name: Main
	 * Descripation: Use to manage web service registration
	 * @author Hardik Mehta(hardik@dazzlebirds.com)
	 * Created date: August 21, 2017
	 */


	defined('BASEPATH') OR exit('No direct script access allowed');

	class Main extends CI_Controller
	{
		/**
		 * function to invoke necessary component
		 * @author Hardik Mehta(hardik@dazzlebirds.com)
		 */

		function __construct()
		{
			parent::__construct();

			$this->load->helper(array('cookie', 'url', 'template', 'sendMail')); 
			$this->load->library('form_validation');
			$this->form_validation->run($this);
		}

		public function index()
		{
			$layout = array('footer'=> 'Elements/main_footer','layoutname' => 'Main/index');
			template($layout);
		}

	}
