<?php
	/**
	 * Controller Name: Dashboard
	 * Descripation: Use to manage web service registration
	 * @author Hardik Mehta(hardik@dazzlebirds.com)
	 * Created date: August 21, 2017
	 */


	defined('BASEPATH') OR exit('No direct script access allowed');

	class Dashboard extends CI_Controller
	{
		/**
		 * function to invoke necessary component
		 * @author Hardik Mehta(hardik@dazzlebirds.com)
		 */

		function __construct()
		{
			parent::__construct();

			$this->checklogin();
			$this->load->model('Dashboard_model');
			$this->load->helper(array('cookie', 'url', 'template', 'sendMail')); 
			$this->load->library('form_validation');
			$this->form_validation->run($this);
		}

		public function index()
		{
			$layout = array('template' => 'Elements/provider_dashboard_template','header'=> 'Elements/p_header','footer'=> 'Elements/p_footer', 'layoutname' => 'Dashboards/provider_dashboard');
			template($layout);
		}

		public function editprofile()
		{
			$layout = array('template' => 'Elements/provider_dashboard_template','header'=> 'Elements/p_header','footer'=> 'Elements/p_footer','layoutname' => 'Dashboards/edit_profile_provider');
			template($layout);
		}

		
		public function Business()
		{
			$this->load->view('ProviderEdit/business');
		}

		public function Businessdata()
		{
			$data = $this->session->userdata('current_user_client');
			$user_id = $data['user_id'];
			$table = 'tbl_users';
			$fields = 'tbl_users.business_name ,tbl_users.service_bio , tbl_users.address, tbl_users.city, tbl_users.state, tbl_users.zip, tbl_users.phone_primary, tbl_users.phone_alternate, tbl_users.crew_member, tbl_users.mile_for_job';
			$return = $this->Dashboard_model->retrive($user_id, $table, $fields);
			echo json_encode($return);
		}

		public function Licensesdata()
		{
			$data = $this->session->userdata('current_user_client');
			$user_id = $data['user_id'];
			$table = 'tbl_users';
			$fields = 'tbl_users.is_full_service_mover , tbl_users.insurance, tbl_users.licensed_movers, tbl_users.business_license_number, tbl_users.business_policy_insurance_number';
			$return = $this->Dashboard_model->retrive($user_id, $table, $fields);
			echo json_encode($return);
		}

		public function ProfilePicdata()
		{
			$data = $this->session->userdata('current_user_client');
			$user_id = $data['user_id'];
			$table = 'tbl_users';
			$fields = 'tbl_users.profile_pic';
			$return = $this->Dashboard_model->retrive($user_id, $table, $fields);
			$return['full_path'] = base_url().'assets/uploads/image/';
			echo json_encode($return);
		}

		public function update()
		{
			$data = $this->session->userdata('current_user_client');
			$user_id = $data['user_id'];
			$table = 'tbl_users';
			$data = $this->input->post();
			$resp = $this->Dashboard_model->update($user_id, $table, $data);
			if ($resp > 0) {
				$response = array('success' => 1,'message' => 'record updated' );
			} else { 
				$response = array('success' => 0,'message' => 'fail to updated record');
			}
			echo json_encode($response);
		}

		public function saveAbout()
		{
			$userdata = $this->session->userdata('current_user_client');
			$user_id = $userdata['user_id'];
			$table = 'tbl_users';
			$field = $this->input->post('field');
			$data[$field] = $this->input->post('about');
			$resp = $this->Dashboard_model->update($user_id, $table, $data);
			if ($resp > 0) {
				$response = array('success' => 1,'message' => 'record updated' );
			} else { 
				$response = array('success' => 0,'message' => 'fail to updated record');
			}
			echo json_encode($response);
		}

		public function ProfilePic()
		{
			$this->load->view('ProviderEdit/photo_upload');
		}
		public function Equipments()
		{
			$this->load->view('ProviderEdit/equipments');
		}
		public function Service()
		{
			$this->load->view('ProviderEdit/service');
		}
		public function Working()
		{
			$this->load->view('ProviderEdit/working');
		}

		public function workingData()
		{
			$data = $this->session->userdata('current_user_client');
			$user_id = $data['user_id'];
			$table = 'tbl_working_hours';
			$fields = '*';
			$join = array(
				array(
					'ref' => 'tbl_days',
					'query' =>  'tbl_days.id = tbl_working_hours.day_id',
					'type' => 'right')
				);

			$return = $this->Dashboard_model->retrive($user_id, $table, $fields, $join);
			echo json_encode($return);
		}

		public function workingHoursupdate()
		{
			$data = $this->session->userdata('current_user_client');
			$user_id = $data['user_id'];
			$table = 'tbl_working_hours';
			$data = $this->input->post();
			$resp = $this->Dashboard_model->workingHoursupdate($user_id, $table, $data);
		}

		public function Truck()
		{
			$this->load->view('ProviderEdit/truck');
		}

		public function Truckdata()
		{
			$data = $this->session->userdata('current_user_client');
			$user_id = $data['user_id'];
			$table = 'tbl_users';
			$fields = 'tbl_users.has_truck , tbl_users.description_truck';
			$return = $this->Dashboard_model->retrive($user_id, $table, $fields);
			echo json_encode($return);
		}
		public function FetchEquipments()
		{
			$data = $this->session->userdata('current_user_client');
			$user_id = $data['user_id'];
			$table = 'tbl_equipment';
			$fields = '*';
			$return['equipments'] = $this->Dashboard_model->retrive($user_id, $table, $fields);
			$table = 'tbl_users';
			$fields = 'tbl_users.equipment_about';
			$return['about'] = $this->Dashboard_model->retrive($user_id, $table, $fields);
			echo json_encode($return);
		}

		public function FetchEquipmentsEdit()
		{
			$id = $this->input->get('id');
			$table = 'tbl_equipment';
			$fields = '*';
			$return = $this->Dashboard_model->retriveEquipment($id, $table, $fields);
			echo json_encode($return);
		}

		public function FetchCredentialEdit()
		{
			$id = $this->input->get('id');
			$table = 'tbl_credential';
			$fields = '*';
			$return = $this->Dashboard_model->retriveEquipment($id, $table, $fields);
			echo json_encode($return);
		}

		public function Credentials()
		{
			$this->load->view('ProviderEdit/credentials');
		}
		public function Licenses()
		{
			$this->load->view('ProviderEdit/licenses');
		}
		public function NewEquipment()
		{
			$this->load->view('ProviderEdit/_new_equipment');
		}

		public function AddNewEquipment()
		{
			$userdata = $this->session->userdata('current_user_client');
			$table = 'tbl_equipment';
			$data = $this->input->post();
			$data['user_id'] = $userdata['user_id'];

			$resp = $this->Dashboard_model->insert($table, $data);
			if ($resp > 0) {
			 	echo json_encode(array('success' => 1, 'message' => 'succefully updated'));	
			} else {
			 	echo json_encode(array('success' => 0, 'message' => 'fail to update'));	
			}
		}

		public function updateEquipment()
		{
			$userdata = $this->session->userdata('current_user_client');
			$table = 'tbl_equipment';
			$data = $this->input->post();
			$id = $data['id'];
			unset($data['id']);

			$resp = $this->Dashboard_model->updateEquipment($id, $table, $data);
			if ($resp > 0) {
			 	echo json_encode(array('success' => 1, 'message' => 'succefully updated'));	
			} else {
			 	echo json_encode(array('success' => 0, 'message' => 'fail to update'));	
			}
		}

		public function updateCredential()
		{
			$userdata = $this->session->userdata('current_user_client');
			$table = 'tbl_credential';
			$data = $this->input->post();
			$id = $data['id'];
			unset($data['id']);

			$resp = $this->Dashboard_model->updateEquipment($id, $table, $data);
			if ($resp > 0) {
				$this->FetchCredential();
			 	// echo json_encode(array('success' => 1, 'message' => 'succefully updated'));	
			} else {
			 	echo json_encode(array('success' => 0, 'message' => 'fail to update'));	
			}
		}

		public function DeleteEquipment()
		{
			$equip_id = $this->input->post('equip_id');
			$this->Dashboard_model->DeleteEquipment($equip_id);
			$this->FetchEquipments();
		}

		public function DeleteCredential()
		{
			$credential_id = $this->input->post('credential_id');
			$this->Dashboard_model->DeleteCredential($credential_id);
			$this->FetchCredential();
		}


		public function NewCredential()
		{
			$this->load->view('ProviderEdit/_new_credential');
		}		

		public function SaveCredential()
		{
			$userdata = $this->session->userdata('current_user_client');
			$table = 'tbl_credential';
			$data = $this->input->post();
			$data['user_id'] = $userdata['user_id'];

			$resp = $this->Dashboard_model->insert($table, $data);
			if ($resp > 0) {
			 	$this->FetchCredential();
			 	// echo json_encode(array('success' => 1, 'message' => 'succefully updated'));	
			} else {
			 	echo json_encode(array('success' => 0, 'message' => 'fail to update'));	
			}
		}

		public function savelatlong()
		{
			$userdata = $this->session->userdata('current_user_client');
			$table = 'tbl_service_location';
			$data = $this->input->post();
			$data['user_id'] = $userdata['user_id'];

			$resp = $this->Dashboard_model->savelatlong($data['user_id'],$table, $data);
			if ($resp > 0) {
			 	echo json_encode(array('success' => 1, 'message' => 'succefully updated'));	
			} else {
			 	echo json_encode(array('success' => 0, 'message' => 'fail to update'));	
			}
		}

		public function FetchCredential()
		{
			$data = $this->session->userdata('current_user_client');
			$user_id = $data['user_id'];
			$table = 'tbl_credential';
			$fields = '*';
			$return['credentials'] = $this->Dashboard_model->retrive($user_id, $table, $fields);
			$table = 'tbl_users';
			$fields = 'tbl_users.credential_about';
			$return['about'] = $this->Dashboard_model->retrive($user_id, $table, $fields);
			echo json_encode($return);
		}
		public function serviceData()
		{
			$data = $this->session->userdata('current_user_client');
			$user_id = $data['user_id'];
			$table = 'tbl_service_location';
			$fields = '*';
			$return = $this->Dashboard_model->retrive($user_id, $table, $fields);
			echo json_encode($return);
		}

		/**
		* @return to check user is logged in or not
		*/
		function checklogin(){
			if($this->session->userdata('current_user_client')==''){
				redirect('Users/login?type=mover');
			}
		}


		function logout(){
			//unset user session

			$this->session->unset_userdata('current_user_client');

			//redirect to login page
			redirect('Users/login?type=mover');
		}

		public function profile()
		{
			$userdata = $this->session->userdata('current_user_client');
			$user_id = $userdata['user_id'];
			$table = 'tbl_credential';
			$fields = '*';
			$data['credential'] = $this->Dashboard_model->retrive($user_id, $table, $fields);
			
			$table = 'tbl_users';
			$fields = 'tbl_users.service_bio,tbl_users.equipment_about,tbl_users.credential_about';
			$data['userdata'] = $this->Dashboard_model->retrive($user_id, $table, $fields);

			$table = 'tbl_equipment';
			$fields = '*';
			$data['equipment'] = $this->Dashboard_model->retrive($user_id, $table, $fields);

			$table = 'tbl_service_location';
			$fields = '*';
			$data['service'] = $this->Dashboard_model->retrive($user_id, $table, $fields);

			$table = 'tbl_working_hours';
			$fields = '*';
			$join = array(
				array(
					'ref' => 'tbl_days',
					'query' =>  'tbl_days.id = tbl_working_hours.day_id',
					'type' => 'right')
				);

			$data['working'] = $this->Dashboard_model->retrive($user_id, $table, $fields, $join);

			// echo "<pre>";
			// print_r($data);exit;

			$layout = array('template' => 'Elements/provider_dashboard_template','header'=> 'Elements/p_header','footer'=> 'Elements/p_footer', 'layoutname' => 'Dashboards/profile',);
			template($layout,$data);
		}

		public function setting()
		{
			$layout = array('template' => 'Elements/provider_dashboard_template','header'=> 'Elements/p_header','footer'=> 'Elements/p_footer', 'layoutname' => 'Dashboards/setting',);
			template($layout);
		}
		
		public function deleteAccountpage()
		{
			$this->load->view('settings/delete_account');
		}
		public function getPaid()
		{
			$this->load->view('settings/get_paid');
		}
		public function resetPassword()
		{
			$this->load->view('settings/reset_password');
		}
		public function updateEmail()
		{
			$this->load->view('settings/update_email');
		}

		public function changePassword()
		{
			$userdata = $this->session->userdata('current_user_client');
			$user_id = $userdata['user_id'];

			$old_password = $this->input->post('old_password');

			$check = $this->Dashboard_model->checkpassword($user_id , $old_password);

			if ($check == 1) {
				$data['password'] = md5($this->input->post('new_password'));
				$table = 'tbl_users';
				$result = $this->Dashboard_model->update($user_id, $table, $data);
				if ($result > 0) {
					$this->session->unset_userdata('current_user_client');
					$response = array('success' => '1', 'message' => 'Password updated successfully');
				} else {
					$response = array('success' => '0', 'message' => 'Something went wrong');
				}
			} else {
				$response = array('success' => '0', 'message' => 'Old password Mismatched');
			}
			echo json_encode($response);
		}

		public function changeEmail()
		{
			$userdata = $this->session->userdata('current_user_client');
			$user_id = $userdata['user_id'];

			$old_password = $this->input->post('password');

			$check = $this->Dashboard_model->checkpassword($user_id , $old_password);
			if ($check == 1) {
				$data['email'] = $this->input->post('new_email');
				$table = 'tbl_users';
				$fields = '*';
				$retrive = $this->Dashboard_model->retriveEmail($data['email'], $table, $fields);
				if (empty($retrive)) {
					$result = $this->Dashboard_model->update($user_id, $table, $data);
					if ($result > 0) {
						$this->session->unset_userdata('current_user_client');
						$response = array('success' => '1', 'message' => 'Email updated successfully');
					} else {
						$response = array('success' => '0', 'message' => 'Something went wrong');
					}
				} else {
					$response = array('success' => '0', 'message' => 'Email is linked with another account');
				}

			} else {
				$response = array('success' => '0', 'message' => 'Authentication Failed');
			}
			echo json_encode($response);
		}
		public function deleteAccount()
		{
			$userdata = $this->session->userdata('current_user_client');
			$user_id = $userdata['user_id'];

			$old_password = $this->input->post('password');

			$check = $this->Dashboard_model->checkpassword($user_id , $old_password);
			if ($check == 1) {
				$data['active'] = 0;
				$table = 'tbl_users';
				$result = $this->Dashboard_model->update($user_id, $table, $data);
				if ($result > 0) {
					$this->session->unset_userdata('current_user_client');
					$response = array('success' => '1', 'message' => 'Email updated successfully');
				} else {
					$response = array('success' => '0', 'message' => 'Something went wrong');
				}

			} else {
				$response = array('success' => '0', 'message' => 'Authentication Failed');
			}
			echo json_encode($response);
		}

		public function upload_image_angular()
		{
			$user_id = $this->session->userdata('current_user_client')['user_id'];
			$config['upload_path']          = $this->config->item('upload_path');
			$config['allowed_types']        = 'gif|jpg|png';
			$config['encrypt_name']         = true;
			$config["allowed_types"]        =  "*";
			
			$config["maintain_ratio"]       =  true;

			$this->load->library('upload', $config);
			$data = $this->upload->initialize($config);

			if ( ! $this->upload->do_upload('image'))
			{
				$error = array('error' => $this->upload->display_errors());
				$response = array('success'=> 0 , 'error' => 01 , 'message' => $error);
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());
				$table = 'tbl_users';
				$imagedata = array('profile_pic' => $data['upload_data']['file_name']);
				$resp = $this->Dashboard_model->update($user_id, $table, $imagedata);
				if ($resp) {
					$picURL = base_url().'assets/uploads/image/'.$data['upload_data']['file_name'];
					$this->session->set_userdata('current_user_client_image',$picURL);
					$response = array('success'=> 1 , 'message' => 'Image uploaded successfully' );
				}else{
					$response = array('success'=> 0 , 'message' => 'Fail to upload' );
				}
			}
			echo json_encode($response);
		}

	}

