<?php
	/**
	 * Controller Name: Users
	 * Descripation: Use to manage web service registration
	 * @author Hardik Mehta(hardik@dazzlebirds.com)
	 * Created date: August 21, 2017
	 */


	defined('BASEPATH') OR exit('No direct script access allowed');

	class Users extends CI_Controller
	{
	/**
	 * function to invoke necessary component
	 * @author Hardik Mehta(hardik@dazzlebirds.com)
	 */

	function __construct()
	{
		parent::__construct();
		
		header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

		if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
		     // The request is using the POST method
			die();
		}

		$this->load->model('User_model');
		$this->load->helper(array('cookie', 'url', 'template', 'sendMail')); 
		$this->load->library('form_validation');
		$this->form_validation->run($this);
	}

	public function userList()
	{
		$userlist = $this->User_model->retriveUserlist();
		echo json_encode($userlist);
	}
	public function userDetails($id)
	{
		$userlist = $this->User_model->retriveUserDetails($id);
		echo json_encode($userlist[0]);
	}
	public function saveUser()
	{
		$data = json_decode(file_get_contents('php://input'), true);

		$savedata['user_id'] = uniqid();
		// $savedata['first_name'] = $data['first_name'];
		// $savedata['last_name'] = $data['last_name'];
		// $savedata['phone_primary'] = $data['phone_primary'];

		$table_name = 'tbl_users';
		$resp = $this->User_model->insert($table_name,$data);
		if ($resp > 0) {
			echo json_encode(array('success'=>'1'));
		} else {
			echo json_encode(array('success'=>'0'));
		}
	}
}
