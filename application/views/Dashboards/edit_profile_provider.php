<style type="text/css">
    #loader-container {
        opacity: 1;
    }
</style>

<!--  For Profile picture   -->
<section id="profile-picture-section" class="settings-section container responsive-bottom-padding" ng-app = "providerEditApp" ng-controller="serviceprovideredit">
    <div id='loader-container' ng-show="loading"><div><img src='<?php echo base_url(); ?>assets/img/loader.gif' alt=' '></div></div>
    <div class="row">
        <div class="col-sm-3 sidebar-col">
            <div>
                <nav class="settings-sidebar">
                    <h4>Profile settings</h4>
                    <ul>
                        <li ng-class="ProfilePicactive" ng-click="ProfilePic()">
                            <a>Profile picture</a>
                        </li>
                        <li ng-class="Businessactive" ng-click="Business()">
                            <a>Business details</a>
                        </li>
                        <li ng-class="Equipmentsactive" ng-click="Equipments()">
                            <a>Equipments details</a>
                        </li>
                        <li ng-class="Serviceactive" ng-click="Service()">
                            <a>Service area</a>
                        </li>
                        <li ng-class="Workingactive" ng-click="Working()">
                            <a>Working hours</a>
                        </li>
                        <li ng-class="Truckactive" ng-click="Truck()">
                            <a>Truck details</a>
                        </li>
                        <li ng-class="Credentialsactive" ng-click="Credentials()">
                            <a>Credentials</a>
                        </li>
                        <li ng-class="Licensesactive" ng-click="Licenses()">
                            <a>licenses</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="col-sm-9 col-xs-12 settings-body">
            <div ng-show="tabShowfx" ng-include="getTab()">
                
            </div>
        </div>
    </div>
</section>
