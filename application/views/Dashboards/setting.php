<style type="text/css">
	footer.has-top-curves {
	    position: relative;
	    margin-top: 310px;
	    padding-top: 380px;
	}
</style>

<div class="main-wrapper" ng-app="setting" ng-controller="accountsetting">
    <div id='loader-container' ng-show="loading"><div><img src='<?php echo base_url(); ?>assets/img/loader.gif' alt=' '></div></div>
    <!--  For Get Get paid   -->
    <section id="account-get-paid-section" class="settings-section container responsive-bottom-padding">
      <div class="row">
        <div class="col-sm-3 sidebar-col">
          <div>
            <nav class="settings-sidebar">
              <h4>Account settings</h4>
              <ul>
                <li ng-class="paidactive">
                  <a href="#account-get-paid-section" ng-click="get_paid()">Get paid</a>
                </li>
                <li ng-class="passwordactive">
                  <a href="#account-update-password-section" ng-click="password_update()">Update password</a>
                </li>
                <li ng-class="emailactive">
                  <a href="#account-update-email-section" ng-click="update_email()">Update email</a>
                </li>
                <li ng-class="deleteactive">
                  <a href="#account-delete-section" ng-click="delete_account()">Delete account</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>


        <div class="col-sm-9 col-xs-12 settings-body"  ng-show="tabShowfx" ng-include="getTab()">
        </div>
      </div>
    </section>
</div>

