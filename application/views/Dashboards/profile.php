<?php $working = $data['working'];?>
<?php $service = $data['service'][0]; ?>
<?php $credential = $data['credential']; ?>
<?php $equipment = $data['equipment']; ?>
<?php $userdata = $data['userdata'][0]; //print_r($userdata);exit; ?>

<div id="nav-container-for-mobile"> </div>
  <div id="wrapper">
    <section id="user-quick-panel" class="container">
      <div class="row">
        <div class="user-info">
          <img src="<?php echo $this->session->userdata('current_user_client_image'); ?>" alt="user-img">
          <div>
            <h4>Perfect choice movers</h4>
            <ul>
              <li>Denvar</li>
              <li>Friday off, 6:00 AM - 8:00 PM</li>
            </ul>
            <a href="<?php echo site_url('Dashboard/editprofile'); ?>" class="btn btn-orange btn-small">
              <i class="fas fa-user-edit"></i> Edit profile</a>
          </div>
        </div>
        <div class="summary-info">
          <ul>
            <li>
              Success score <p class="success-percentage">0%</p>
              <div class="success-score success-score-90">

                <div></div>
              </div>
            </li>
            <li>
              <p>Moved : <span>0</span></p>
            <li>
              <p>Reviews : <span>0</span></p>
            </li>
          </ul>
        </div>
      </div>
    </section>

    <section class="container content-wrapper ">
      <div class="row center-xs">
        <div class="col-xs-12 content profile-page-content">
          <section class="summary-profile-section">
            <h4>About Us </h4>
            <p><?php echo $userdata['service_bio'] ?>
            </p>
            <p class="property">
              <span>0</span>
              Moved
            </p>
            <p class="property">
              <span>0%</span>
              Rehire
            </p>
            <p class="property">
              <span>0/5</span>
              Rating
            </p>

          </section>

          <section class="equipment-profile-section">
            <div class="head">
              <h4>Equipment details</h4>
            </div>

            <p><?php echo $userdata['equipment_about'] ?>
            </p>


            <div class="loop-equipment">
              <div class="items-list type-1">
              	<?php foreach ($equipment as $value) { ?>
              		<?php if($value['is_included'] == 0) { ?>
		                <div class="item">
		                  <i class="fas fa-check"></i>
		                  <h5><?php echo $value['item_name']; ?>:</h5>
		                  <p><?php echo $value['item_description']; ?></p>
		                  <p>[always included, no fee]</p>
		                </div>
	              	<?php } ?>
              	<?php } ?>

              </div>
              <div class="items-list type-2">
              	<?php foreach ($equipment as $value) { ?>
              		<?php if($value['is_included'] == 1) { ?>
		                <div class="item">
		                  <i class="fas fa-dollar-sign"></i>
		                  <h5><?php echo $value['item_name']; ?>:</h5>
		                  <p><?php echo $value['item_description']; ?></p>
		                  <p>[flat price, <?php echo $value['price']; ?>]</p>
		                </div>
	              	<?php } ?>
              	<?php } ?>
              </div>
            </div>


          </section>

          <section class="service-area-profile-section">
            <div class="head">
              <h4>Service area</h4>
            </div>

            <div class="map-container">
				      <iframe width="100%" height="350" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=<?= $service['latitude'] ?>,<?= $service['longitude'] ?>&key=AIzaSyA4dXiCdayvN1jBkLMK_07J7oRNqt9xg3Y&zoom=15"></iframe>
            </div>

            <p>Affordable Professional Movers is based in Fairport, NY and covers a 50 mile radius.
              <br> <a href="#service-area-cities">Travel fees are included in the prices you see.</a>
            </p>

            <h4>
              Service Area includes
            </h4>

            <div id="service-area-cities">
        		<?php $manual_location = explode(",",$service['manual_location']); ?>
              <ul id="cities-menu">
                <?php foreach ($manual_location as $value) { ?>
                  <li><?php echo $value; ?></li>
                <?php } ?>
              </ul>
            </div>
          </section>

          <section class="working-hours-profile-section">
            <div class="head">
              <h4>Working hours</h4>
            </div>
            <h3>Available working hours <br>
              <small>With central time</small>
            </h3>

            <div class="working-hours-time-table row">
              <div class="col-md-5 col">
                <ul>
                	<?php foreach ($working as $value){ ?>
	                  <li>
	                    <p class="day"><?php echo $value['name']; ?></p>
	                    <div class="time">
                        <?php if($value['is_off'] == 0) { ?>
                          <span class="from"><?php echo $value['hours_from']; ?></span> -
                          <span class="to"><?php echo $value['hours_to']; ?> </span>
                        <?php } else { ?>
                          <span class="from">Day Off </span>
  	                    <?php } ?>
                      </div>
	                  </li>
                	<?php } ?>	
                </ul>
              </div>
              <div class="col-md-7 col">
                <img src="<?php echo base_url(); ?>assets/img/clock.jpg" alt="Clock">
              </div>
            </div>
          </section>

          <section class="credentials-profile-section">
            <div class="head">
              <h4>Licence & other business inforamtion </h4>
            </div>

            <p><?php echo $userdata['credential_about'] ?></p>

            <div class="credentials-container row">
              
            	<?php foreach ($credential as $key => $value) { ?>
      					<div class="credential-item col-md-6 col-sm-12">
      						<h5>Insured business</h5>
      						<?php if ($value['has_policy_date'] == 1) { ?>
      						<p class="policy-number check-empty">Policy number : <span><?php echo $value['credential_policy_number']; ?></span></p>
      						<?php } ?>
      						<?php if ($value['has_policy_issuer'] == 1) { ?>
      						<p class="policy-issuer check-empty">Policy issuer : <span><?php echo $value['credential_policy_issuer']; ?></span></p>
      						<?php } ?>
      						<?php if ($value['has_description'] == 1) { ?>
      							<p class="policy-description check-empty"><?php echo $value['credential_description']; ?></p>
      						<?php } ?>
      					</div>
            	<?php } ?>

            </div>
          </section>


        </div>
      </div>
    </section>


    <section class="container content-wrapper clients-reviews-container">
      <h3>Work history & Feedback <br>
        <small>
          260 Customer reviews
        </small></h3>

      <select name="" id="reviews-sorting">
        <option value="">Newest First</option>
        <option value="">Oldest First</option>
        <option value="">High budget</option>
        <option value="">Low budget</option>
        <option value="">Hight rating</option>
        <option value="">Low rating</option>
      </select>


      <div class="clients-reviews">


        <div class="review row">
          <div class="col-md-9 col-sm-12 main-review-info">
            <h4 class="review-client-name">
              Danielle H.
            </h4>
            <span class="review-budget">
              $400
            </span>
            <div class="review-info">
              <div class="review-rating"><span class="review-stars"><span class="stars-45 actual-rate-stars"><img src="<?php echo base_url(); ?>assets/img/review-rating-template.png"
                      alt=""></span></span> <span>4.5</span>
              </div>
              <div class="job-location"><span>Pittsford, NY - 6/24/2018</span></div>
            </div>
            <div class="review-body">
              <p class="review">
                They were on time... Worked hard and fast... Made an effort to make sure I was pleased and happy with
                their
                work. And completed loading my pod in just under the 3 hours quoted. REALLY nice people to wo
                rk with too.. And very good at packing furniture for a long journey. I would recommend them to my
                family and
                friends. .. <a href="#">Read More</a>
              </p>

              <div class="replay">
                <span> <img src="<?php echo base_url(); ?>assets/img/replay.png" alt=""> <br>
                  Replay to client :</span>
                <p> “ Thanks a lote for your postive feedback, Glad to serve you,
                  Have a nice weekend Danielle ” </p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-12 job-scope">
            <ul>
              <li>4 Hours</li>
              <li>2 Helpers</li>
              <li>Truck</li>
              <li>Loading</li>
              <li>Unloadin</li>
              <li>
                < 400 lb itmes</li> <li>No damage
              </li>
            </ul>
          </div>
        </div>

        <div class="review row">
          <div class="col-sm-9 main-review-info">
            <h4 class="review-client-name">
              Danielle H.
            </h4>
            <span class="review-budget">
              $400
            </span>
            <div class="review-info">
              <div class="review-rating"><span class="review-stars"><span class="stars-45 actual-rate-stars"><img src="<?php echo base_url(); ?>assets/img/review-rating-template.png"
                      alt=""></span></span> <span>4.5</span>
              </div>
              <div class="job-location"><span>Pittsford, NY - 6/24/2018</span></div>
            </div>
            <div class="review-body">
              <p class="review">
                They were on time... Worked hard and fast... Made an effort to make sure I was pleased and happy with
                their
                work. And completed loading my pod in just under the 3 hours quoted. REALLY nice people to wo
                rk with too.. And very good at packing furniture for a long journey. I would recommend them to my
                family and
                friends. .. <a href="#">Read More</a>
              </p>

              <div class="replay">
                <span> <img src="<?php echo base_url(); ?>assets/img/replay.png" alt=""> <br>
                  Replay to client :</span>
                <p> “ Thanks a lote for your postive feedback, Glad to serve you,
                  Have a nice weekend Danielle ” </p>
              </div>
            </div>
          </div>
          <div class="col-sm-3 job-scope">
            <ul>
              <li>4 Hours</li>
              <li>2 Helpers</li>
              <li>Truck</li>
              <li>Loading</li>
              <li>Unloadin</li>
              <li>
                < 400 lb itmes</li> <li>No damage
              </li>
            </ul>
          </div>
        </div>


        <div class="review row">
          <div class="col-sm-9 main-review-info">
            <h4 class="review-client-name">
              Danielle H.
            </h4>
            <span class="review-budget">
              $400
            </span>
            <div class="review-info">
              <div class="review-rating"><span class="review-stars"><span class="stars-45 actual-rate-stars"><img src="<?php echo base_url(); ?>assets/img/review-rating-template.png"
                      alt=""></span></span> <span>4.5</span>
              </div>
              <div class="job-location"><span>Pittsford, NY - 6/24/2018</span></div>
            </div>
            <div class="review-body">
              <p class="review">
                They were on time... Worked hard and fast... Made an effort to make sure I was pleased and happy with
                their
                work. And completed loading my pod in just under the 3 hours quoted. REALLY nice people to wo
                rk with too.. And very good at packing furniture for a long journey. I would recommend them to my
                family and
                friends. .. <a href="#">Read More</a>
              </p>

              <div class="replay">
                <span> <img src="<?php echo base_url(); ?>assets/img/replay.png" alt=""> <br>
                  Replay to client :</span>
                <p> “ Thanks a lote for your postive feedback, Glad to serve you,
                  Have a nice weekend Danielle ” </p>
              </div>
            </div>
          </div>
          <div class="col-sm-3 job-scope">
            <ul>
              <li>4 Hours</li>
              <li>2 Helpers</li>
              <li>Truck</li>
              <li>Loading</li>
              <li>Unloadin</li>
              <li>
                < 400 lb itmes</li> <li>No damage
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>


    <script type="text/javascript">
      splittext();
    </script>