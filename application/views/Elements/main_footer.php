
  <footer>
    <div class="container-fluid">
      <div class="row center-xs">
        <div class="col-md-3 col-sm-12 break-width">
          <a href="#">
            <div class="footer-site-logo"></div>
          </a>
          <p>
            This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vetl velit auctor aliquet.
          </p>
          <nav class="social-links">
            <ul>
              <li>
                <a href="#">
                  <i class="fab fa-twitter fa-lg"></i>
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="fab fa-facebook-f fa-lg"></i>
                </a>
              </li>
              <li>
                <a href="#">

                  <i class="fab fa-google-plus-g fa-lg"></i>

                </a>
              </li>
            </ul>
          </nav>
          <span class="social-line"></span>




        </div>
        <div class="col-md-2 col-xs-4 break-width">
          <h4>Home</h4>

          <nav>
            <ul>
              <li>
                <a href="#">Search for a helper</a>
              </li>
              <li>
                <a href="#">Education</a>
              </li>
              <li>
                <a href="#">How it works</a>
              </li>
              <li>
                <a href="#">Client’s area</a>
              </li>
              <li>
                <a href="#">Join us</a>
              </li>
            </ul>
          </nav>
        </div>



        <div class="col-md-2 col-xs-4  break-width">
          <h4>Help</h4>
          <nav>
            <ul>
              <li>
                <a href="#">Help topics</a>
              </li>
              <li>
                <a href="#">FAQ’s</a>
              </li>
              <li>
                <a href="#">Support</a>
              </li>
              <li>
                <a href="#">Contact us</a>
              </li>
              <li>
                <a href="#">Privacy policy</a>
              </li>
              <li>
                <a href="#">Terms of use</a>
              </li>
            </ul>
          </nav>
        </div>

        <div class="col-md-2 col-xs-4  break-width">
          <h4>New helper ?</h4>
          <nav>
            <ul>
              <li>
                <a href="#">Create account</a>
              </li>
              <li>
                <a href="#">Fees</a>
              </li>
              <li>
                <a href="#">Documentations</a>
              </li>
              <li>
                <a href="#">Payments</a>
              </li>
            </ul>
          </nav>
        </div>

        <div class="col-md-3 col-sm-12 search-col">
          <h4>Subscribe for news letter
            <small>No spam, we promise</small>
          </h4>
          <form class="search-box-group" action="" name="search-form">
            <input type="email" name="search-box" class="textbox" id="search-box" placeholder="Your Email">
            <button type="submit">Subscribe
            </button>
          </form>


          <nav class="app-links">
            <h4>Get Our App</h4>
            <ul>
              <li>
                <a href="#">
                  <i class="fab fa-apple fa-lg"></i>
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="fab fa-android fa-lg"></i>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="row center-xs copy-rights bottom-xs">
        <div class="col-sm-4">
          <span>Copy right 2018 © MyMovingLabor.com All rights are reserved</span>
        </div>
      </div>
    </div>
  </footer>
  
  </div>