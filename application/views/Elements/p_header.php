<header class="has-light-header-curve">
    <div class="light-header-curve">
    </div>
    <div id="main-nav-wrapper" class="">
      <div class="site-logo">
        <a href="<?php echo site_url() ?>"></a>
      </div>
      <div class="menu-container">
        <div id="normal-nav-container">
          <nav class="main-nav" role="navigation">
            <!-- Sample menu definition -->
            <ul id="main-menu" class="sm sm-clean">
              <li>
                <a href="#" class="">Dashboard</a>
              </li>
              <li>
                <a href="<?php echo site_url('Dashboard/profile'); ?>" class="">Profile</a>
                <!-- <ul>
                  <li>
                    <a href="#" class="">Edit profile</a>
                  </li>
                </ul> -->
              </li>
              <li>
                <a href="#" class="">Messages</a>
              </li>
              <li>
                <a href="#" class="">Orders</a>
              </li>
              <li>
                <a href="#" class="has-image">Account
                  <img class="user-img" src="<?php echo $this->session->userdata('current_user_client_image'); ?>" alt="user-name"> </a>
                <ul>

                  <li>
                    <a href="<?php echo site_url('Dashboard/setting'); ?>" class="">Settings</a>
                  </li>
                  <li>
                    <a href="" class="">Support</a>
                  </li>
                  <li>
                    <a href="<?php echo site_url('Dashboard/logout'); ?>" class="">Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="nav-buttons">
        <!-- Mobile menu toggle button (hamburger/x icon) -->
        <a href="#" class="toggle-mobile-nav-btn collabsed">
          <span class="bars">
            <i class="fas fa-bars"></i>
          </span>
          <span class="x">
            <i class="fas fa-times"></i>
          </span>
        </a>
      </div>
    </div>
    <div id="nav-container-for-mobile"> </div>
  </header>