<header class="has-light-header-curve">
    <div class="light-header-curve">
    </div>
    <div id="main-nav-wrapper" class="">
      <div class="site-logo">
        <a href="index.html"></a>
      </div>
      <div class="menu-container">
        <div id="normal-nav-container">
          <nav class="main-nav" role="navigation">
            <!-- Sample menu definition -->
            <ul id="main-menu" class="sm sm-clean">
              <li>
                <a href="search-helpers.html" class="">Find a helper</a>
              </li>
              <li>
                <a href="about.html" class="">About</a>
              </li>
              <li>
                <a id="how-it-works-btn" href="#" class="">How it works</a>
              </li>
              <li>
                <a href="#" class="">Login</a>
                <ul>
                  <li><a class="" href="<?php echo site_url('users/login?type=client'); ?>">Clients area</a></li>
                  <li><a class="active" href="<?php echo site_url('users/login?type=mover'); ?>">Login as a Helper</a></li>
                </ul>
              </li>
              <li>
                <a href="#" class="">Join us</a>
                <ul>
                  <li><a class=""href="<?php echo site_url('users/signup?type=client'); ?>">New Client</a></li>
                  <li><a class="" href="<?php echo site_url('users/signup?type=mover'); ?>">New Service provider</a></li>
                </ul>
              </li>
              <li>
                <a href="support.html" class="">Support</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="nav-buttons">
        <!-- Mobile menu toggle button (hamburger/x icon) -->
        <a href="#" class="toggle-mobile-nav-btn collabsed">
          <span class="bars">
            <i class="fas fa-bars"></i>
          </span>
          <span class="x">
            <i class="fas fa-times"></i>
          </span>
        </a>
      </div>
    </div>
    <div id="nav-container-for-mobile"> </div>
  </header>