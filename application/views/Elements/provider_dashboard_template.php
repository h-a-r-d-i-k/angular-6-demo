<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>My Moving Labor</title>

  <!--StyleSheets-->
  <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

  <!--Google fonts-->
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Open+Sans:400,600,700,800" rel="stylesheet">

  <!--FlexBox-->
  <link href="<?php echo base_url(); ?>assets/node_modules/flexboxgrid/css/flexboxgrid.min.css" rel="stylesheet">

  <!--normalizer-->
  <link href="<?php echo base_url(); ?>assets/node_modules/normalize.css/normalize.css" rel="stylesheet">

  <!--Font-awesome-->
  <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe"
    crossorigin="anonymous"></script>

  <!-- SmartMenus core CSS (required) -->
  <link href="<?php echo base_url(); ?>assets/css/sm-core-css.css" rel="stylesheet" type="text/css" />

  <link href="<?php echo base_url(); ?>assets/css/angular-toastr.css" rel="stylesheet" type="text/css" />
  
  <!-- "sm-mint" menu theme (optional, you can use your own CSS, too) -->
  <link href="<?php echo base_url(); ?>assets/css/sm-clean/sm-clean.css" rel="stylesheet" type="text/css" />
  
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.2/angular.min.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js" ></script>
</head>

<body>
    <?php if (isset($header)) { $this->load->view($header); } ?>

    <?php if (isset($left_nav)) { $this->load->view($left_nav); } ?>

    <?php if (isset($partial)) { $this->load->view($partial); } ?>

    <?php if (isset($footer)) { $this->load->view($footer); } ?>

  <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-3.6.0.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
  <script>
    window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-3.3.1.min.js"><\/script>')
  </script>

  <!-- SmartMenus jQuery plugin -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.smartmenus.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/node_modules/jquery-form-validator/form-validator/jquery.form-validator.min.js"></script>
  
  
  <!-- Js codes -->

  <script src="<?php echo base_url(); ?>assets/js/angular-toastr.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/angular-toastr.tpls.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/setting.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/edit_profile.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
</body>

</html>


