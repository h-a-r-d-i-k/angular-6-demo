<!-- Change property starts-->
          <form action="#" method="get">
            <div class="row head">
              <h4>Available working hours</h4>
            </div>

            <div class="col-sm-12">
              <div class="input-field working-hours" >

                <div class="day-working" ng-repeat="work in works" >
                  <h5>{{ work.name }}</h5>
                  <label class="day-off-label" ng-show="work.is_off == 1">
                    <input type="checkbox" class="day-off" ng-true-value="1" ng-false-value="0"  ng-model="worked[$index].is_off" ng-checked="true">Day off
                  </label>
                  <label class="day-off-label" ng-show="work.is_off == 0">
                    <input type="checkbox" class="day-off" ng-true-value="1" ng-false-value="0"  ng-model="worked[$index].is_off"  ng-checked="false">Day off
                  </label>
                  <div class="time-pickers">
                    <p>From</p>
                    <input class="timepicker-work textbox" id="from_{{work.day_id}}" ng-model="worked[$index].hours_from">
                    <p>To</p>
                    <input class="timepicker-work textbox" id="to_{{work.day_id}}" ng-model="worked[$index].hours_to">
                  </div>
                </div>

              </div>
            </div>


            <button class="btn btn-orange" type="button" ng-click="SaveWorking(worked)" >Save</button>
          </form>
