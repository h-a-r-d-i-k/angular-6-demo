<!-- <form action="#" method="get"> -->
              <div class="row head">
                <h4>Credentials & other business information</h4>
              </div>
              <div class="col-sm-12">
                <div class="input-field">
                  <textarea class="textbox " rows="6" name="equipments-summary" ng-model="credential_about">If you would like us to bring any of the equipment listed, make sure to put this request in your order details when placing your order. If additional fees are listed for this equipment, you must arrange payment for these items directly with us after your order is placed.</textarea>
                </div>
                  <button class="btn btn-orange" type="button" ng-click="save_about(credential_about, 'credential_about')">Save</button>


                <div class="all-credentials"  ng-repeat="credential in credentials">

                  <div class="credential">
                    <i class="fas fa-check"></i>
                    <h5>{{credential.name}}:
                      <button class="btn btn-small btn-default" id="edit-equipment" ng-click="update_credential(credential.id)">
                        <i class="fas fa-pencil-alt"></i>
                      </button>
                      <button class="btn btn-small btn-default" id="delete-equipment" ng-click="delete_credential(credential.id)">
                        <i class="fas fa-trash-alt"></i>
                      </button>
                    </h5>
                    <p ng-show="credential.has_description == 1">{{ credential.credential_description }}</p>
                    <p ng-show="credential.has_policy_date == 1">Policy number
                      <span>{{ credential.credential_policy_number }}</span>
                    </p>
                    <p ng-show="credential.has_policy_issuer == 1">Policy issuer
                      <span>{{ credential.credential_policy_issuer }}</span>
                    </p>
                  </div>

                </div>


              </div>
              <div class="col-sm-12 add-new-credential">


                <div class="row head">
                  <h4>
                    <i class="fas fa-plus"></i> Add a new credential</h4>
                </div>

                <div class="input-field collapse-container" ng-show="blockShowfx">
                  <h5>Credential name</h5>
                  <input type="text" class="textbox " name="new-credential-name" ng-model="credential_name">

                  <div class="checkboxes-container">

                    <label>
                      <input type="checkbox" name="" class="credential-option" id="description_checkbox" ng-true-value="1" ng-false-value="0" ng-checked="description_checkbox" ng-click="updateCheckbox(description_checkbox , 'desc')" ng-model="description_checkbox" string-to-number>Has description
                    </label>

                    <label>
                      <input type="checkbox" name="" class="credential-option" id="credential-policy-number"  ng-true-value="1" ng-false-value="0" ng-checked="number_checkbox" ng-click="updateCheckbox(number_checkbox , 'number')" ng-model="number_checkbox" string-to-number>Has policy Number
                    </label>

                    <label>
                      <input type="checkbox" name="" class="credential-option" id="credential-policy-issuer"  ng-true-value="1" ng-false-value="0" ng-checked="issuer_checkbox" ng-click="updateCheckbox(issuer_checkbox , 'issuer')" ng-model="issuer_checkbox" string-to-number>Has Policy issuer
                    </label>
                  </div>


                  <div class="new-credential-desc-container" ng-style="credential_desc_style">
                    <h5>Credential description</h5>
                    <textarea class="textbox new-credential-desc" value="" ng-model="credential.desc"></textarea>
                  </div>

                  <div class="new-credential-policy-number-container"  ng-style="credential_number_style">
                    <h5>Credential policy number</h5>
                    <input type="text" class="textbox new-credential-policy-number" value="" ng-model="credential.number">
                  </div>

                  <div class="new-credential-policy-issuer-container"  ng-style="credential_issuer_style">
                    <h5>Credential policy issuer</h5>
                    <input type="text" class="textbox new-credential-policy-issuer" value="" ng-model="credential.issuer">
                  </div>



                </div>



                <button class="expand btn btn-default btn-small" ng-click="add_new_credential()">
                  Add new credential
                </button>

                <button class="collapse btn btn-default btn-small"  ng-style="remove_new_credential_style" ng-click="remove_new_equipment()">
                  <i class="fas fa-times"></i>
                </button>

              </div>

<button class="btn btn-orange" type="button" ng-click="SaveCredential(credential_name, credential, description_checkbox,number_checkbox,issuer_checkbox)"  ng-show="clickedCredential == 1">Save</button>
<button class="btn btn-orange" type="button" ng-click="add_new_credential()"  ng-show="clickedCredential == 0">Save</button>

            <!-- </form> -->