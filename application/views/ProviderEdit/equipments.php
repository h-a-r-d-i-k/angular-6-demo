 <!-- <form action="#" method="get"> -->
            <div class="row head">
              <h4>About your equipments</h4>
            </div>
            <div class="col-sm-12">
              <div class="input-field">
                <textarea class="textbox " rows="6" name="equipments-summary"  ng-model="equipment_about">If you would like us to bring any of the equipment listed, make sure to put this request in your order details when placing your order. If additional fees are listed for this equipment, you must arrange payment for these items directly with us after your order is placed.</textarea>
              </div>
              <button class="btn btn-orange" type="button" ng-click="save_about(equipment_about, 'equipment_about')">Save</button>

              <div class="all-equipments">
                <div ng-repeat="equip in equipments">
                  <div class="equipment included" ng-show="equip.is_included == 1">
                    <i class="fas fa-check"></i>
                    <h5>{{ equip.item_name }}:
                      <button class="btn btn-small btn-default" id="edit-equipment" ng-click="update_equipment(equip.id)">
                        <i class="fas fa-pencil-alt"></i>
                      </button>
                      <button class="btn btn-small btn-default" id="delete-equipment" ng-click="delete_equipment(equip.id)">
                        <i class="fas fa-trash-alt"></i>
                      </button>
                    </h5>
                    <p>{{ equip.item_description }}</p>
                    <p>[always included, no fee]</p>
                  </div>

                  <div class="equipment paid"  ng-show="equip.is_included == 0">
                    <i class="fas fa-dollar-sign"></i>
                    <h5>{{ equip.item_name }}:
                      <button class="btn btn-small btn-default" id="edit-equipment" ng-click="update_equipment(equip.id)" >
                        <i class="fas fa-pencil-alt"></i>
                      </button>
                      <button class="btn btn-small btn-default" id="delete-equipment" ng-click="delete_equipment(equip.id)">
                        <i class="fas fa-trash-alt"></i>
                      </button>
                    </h5>
                    <p>{{ equip.item_description }}</p>
                    <p>[Flat price: ${{ equip.price }}]</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12">

              <div class="row head">
                <h4>
                  <i class="fas fa-plus"></i> Add new equipment</h4>
              </div>

              <div class="collapse-container" ng-show="blockShowfx">
                <div class="input-field">
                  <h5>Equipment name</h5>
                  <input type="text" class="textbox" id="equipment_name" name="new-equipment-name" ng-model="equipment_name">
                </div>

                <div class="input-field">
                  <h5>Equipment information</h5>
                  <textarea class="textbox " name="new-equipment-info" id="equipment_info" ng-model="equipment_info"></textarea>
                </div>

                <div class="input-field new-equipment-type">

                  <input type="radio" ng-click="equipmentChoice(1)" checked name="new-equipment-type" id="new-equipment-is-included" value="Equipment is included" ng-checked="price_included">
                  <label class="radio-label" for="new-equipment-is-included">
                    <i class="fas fa-check"></i> Equipment is included
                  </label>
                  <input type="radio" name="new-equipment-type"  ng-click="equipmentChoice(0)" id="new-equipment-has-price" value="Equipment has flat price" ng-checked="price_included_not">
                  <label class="radio-label" for="new-equipment-has-price">
                    <i class="fas fa-dollar-sign"></i> Equipment has flat price</label>

                </div>

                <div class="input-field new-equipment-price-container" id="flat_price_style" ng-style="flat_price_style">
                  <h5>Flat price</h5>
                  <input type="tel" id="new-equipment-price" name="new-equipment-price" value="100" class="textbox" ng-model="equipment_price" string-to-number>
                </div>
              </div>

              <button class="expand btn btn-default btn-small" ng-click="add_new_equipment()">
                Add new equipment
              </button>

              <button class="collapse btn btn-default btn-small" ng-style="remove_new_equipment_style" ng-click="remove_new_equipment()">
                <i class="fas fa-times"></i>
              </button>

            </div>
            <button class="btn btn-orange" type="button" ng-click="SaveEquipment(equipment_name,equipment_info,equipment_price)" ng-show="clickedEquipment == 1">Save</button>
            <button class="btn btn-orange" type="button" ng-click="add_new_equipment()" ng-show="clickedEquipment == 0">Save</button>

          <!-- </form> -->