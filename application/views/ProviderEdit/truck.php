<form action="#" method="get">
    <div class="row head">
      <h4>Truck details</h4>
    </div>
    <div class="col-sm-12">
      <div class="input-field truck-details">
        <label ng-show="has_truck == 1">
          <input type="checkbox" id="has-truck" ng-true-value="1" ng-false-value="0" ng-checked="true" ng-model="has_truck" ng-change="truck_changed(has_truck)">We own a truck
        </label>
        <label ng-show="has_truck == 0">
          <input type="checkbox" id="has-truck" ng-true-value="1" ng-false-value="0" ng-checked="false" ng-model="has_truck" ng-change="truck_changed(has_truck)">We own a truck
        </label>

        <textarea rows="5" class="textbox " name="truck-summary" ng-model="description_truck" ng-style="truck_detail_style">If you would like us to bring any of the equipment listed, make sure to put this request in your order details when placing your order. If additional fees are listed for this equipment, you must arrange payment for these items directly with us after your order is placed.</textarea>
      </div>

    </div>

    <button class="btn btn-orange" type="button" ng-click="SaveTruck(has_truck, description_truck)">Save</button>

  </form>
