                <div class="input-field">
                  <h5>Equipment name</h5>
                  <input type="text" class="textbox" id="equipment_name" name="new-equipment-name" ng-model="equipment_name">
                </div>

                <div class="input-field">
                  <h5>Equipment information</h5>
                  <textarea class="textbox " name="new-equipment-info" id="equipment_info" ng-model="equipment_info"></textarea>
                </div>

                <div class="input-field new-equipment-type">

                  <input type="radio" ng-click="equipmentChoice(1)" checked name="new-equipment-type" id="new-equipment-is-included" value="Equipment is included" ng-checked="price_included">
                  <label class="radio-label" for="new-equipment-is-included">
                    <i class="fas fa-check"></i> Equipment is included
                  </label>
                  <input type="radio" name="new-equipment-type"  ng-click="equipmentChoice(0)" id="new-equipment-has-price" value="Equipment has flat price" ng-checked="price_included_not">
                  <label class="radio-label" for="new-equipment-has-price">
                    <i class="fas fa-dollar-sign"></i> Equipment has flat price</label>

                </div>

                <div class="input-field new-equipment-price-container" id="flat_price_style" ng-style="flat_price_style">
                  <h5>Flat price</h5>
                  <input type="tel" id="new-equipment-price" name="new-equipment-price" value="100" class="textbox" ng-model="equipment_price" string-to-number>
                </div>

