
<form action="#" method="get">
	<div class="row head">
	  	<h4>Business information</h4>
	</div>

	<div class="input-field">
		<h5>Business name</h5>
		<input type="text" class="textbox" name="primary-phone" ng-model="business_name" placeholder="Your business name">
	</div>


	<div class="input-field">
		<h5>Your Service Bio</h5>
		<textarea class="textbox " rows="6" name="business-bio" placeholder="Business summary" ng-model="service_bio"></textarea>
	</div>

	<div class="input-field inline-input ">
		<h5>Primary phone</h5>
		<input ng-model="phone_primary" type="tel" class="textbox " name="primary-phone" placeholder="Primary phone">
	</div>

	<div class="input-field inline-input">
		<h5>Alternate phone</h5>
		<input ng-model="phone_alternate" type="tel" class="textbox " name="alternate-phone" placeholder="Alternate phone">
	</div>


	<div class="row head">
		<h4>Business address</h4>
	</div>


	<div class="input-field inline-input">
		<h5>Address</h5>
		<input ng-model="address" type="text" class="textbox " name="business-address"  placeholder="Business address">
	</div>

	<div class="input-field inline-input">
		<h5>City</h5>
		<input ng-model="city" type="text" class="textbox " name="business-city"  placeholder="Business address">
	</div>

	<div class="input-field inline-input">
		<h5>State</h5>

		<select name="state" ng-model="state" class="textbox">
			<option value="">State</option>
			<option value="AL">Alabama</option>
			<option value="AK">Alaska</option>
			<option value="AZ">Arizona</option>
			<option value="AR">Arkansas</option>
			<option value="CA">California</option>
			<option value="CO" selected="selected">Colorado</option>
			<option value="CT">Connecticut</option>
			<option value="DE">Delaware</option>
			<option value="DC">District Of Columbia</option>
			<option value="FL">Florida</option>
			<option value="GA">Georgia</option>
			<option value="HI">Hawaii</option>
			<option value="ID">Idaho</option>
			<option value="IL">Illinois</option>
			<option value="IN">Indiana</option>
			<option value="IA">Iowa</option>
			<option value="KS">Kansas</option>
			<option value="KY">Kentucky</option>
			<option value="LA">Louisiana</option>
			<option value="ME">Maine</option>
			<option value="MD">Maryland</option>
			<option value="MA">Massachusetts</option>
			<option value="MI">Michigan</option>
			<option value="MN">Minnesota</option>
			<option value="MS">Mississippi</option>
			<option value="MO">Missouri</option>
			<option value="MT">Montana</option>
			<option value="NE">Nebraska</option>
			<option value="NV">Nevada</option>
			<option value="NH">New Hampshire</option>
			<option value="NJ">New Jersey</option>
			<option value="NM">New Mexico</option>
			<option value="NY">New York</option>
			<option value="NC">North Carolina</option>
			<option value="ND">North Dakota</option>
			<option value="OH">Ohio</option>
			<option value="OK">Oklahoma</option>
			<option value="OR">Oregon</option>
			<option value="PA">Pennsylvania</option>
			<option value="RI">Rhode Island</option>
			<option value="SC">South Carolina</option>
			<option value="SD">South Dakota</option>
			<option value="TN">Tennessee</option>
			<option value="TX">Texas</option>
			<option value="UT">Utah</option>
			<option value="VT">Vermont</option>
			<option value="VA">Virginia</option>
			<option value="WA">Washington</option>
			<option value="WV">West Virginia</option>
			<option value="WI">Wisconsin</option>
			<option value="WY">Wyoming</option>
		</select>
	</div>
	<div class="input-field inline-input">
		<h5>Zip code</h5>
		<input ng-model="zip" type="text" class="textbox " name="business-zip" placeholder="Zip code">
	</div>
	<div class="input-field inline-input inline-input-small">
		<h5>How many crew members can you provide ?</h5>
		<input string-to-number ng-model="crew_member" class="input-numbers textbox" type="tel" maxlength="2" max="100" name="crew_members">
	</div>
	<div class="input-field inline-input  inline-input-small">
		<h5>How many miles can you travel for jobs ?</h5>
		<input string-to-number ng-model="mile_for_job"class="input-numbers textbox" type="tel" name="moving_miles">
	</div>
	<button class="btn btn-orange" type="button" ng-click="SaveBusiness(address,business_name,city,crew_member,mile_for_job,phone_alternate,phone_primary,state,zip,service_bio)">Save</button>
</form>



