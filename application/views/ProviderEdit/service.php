 <!-- Change property starts-->
          <!-- <form action="#" method="get"> -->
            <div class="row head">
              <h4>Service area</h4>
            </div>

            <div class="col-sm-12">
              <div class="input-field service-area">
                <input type="text" class="textbox" name="service-location" ng-model="currentLocation" placeholder="Your service location">
                <button id="get-location" class="btn btn-default" ng-click="getLocation()">Get location</button>
                <p>
                  <i class="fas fa-map-marker-alt"></i> Your service covers a 50 mile radius.</p>
              </div>
              <div id="editmaps">
              </div>
            </div>

            <div class="row head">
              <h4>Service Area includes</h4>
            </div>
            <div class="col-sm-12">
              <div class="input-field">
                <p>Add your service areas each separated with a comma</p>
                <textarea name="service-areas" class="textbox" cols="30" rows="10" ng-model="manual_location"></textarea>

              </div>
            </div>
            <button class="btn btn-orange" type="button" ng-click="saveManual(manual_location)">Save</button>
          <!-- </form> -->
