        <form action="#" method="get">
            <div class="row head">
              <h4>Licenses</h4>
            </div>



            <div class="col-sm-12 licenses-container">

              <div class="input-field has-licence-option">
                <P>Do you carry a valid business license ?</P>
                <small>If approved you will be asked for your license number. </small>

                <select name="has_valid_business_license" class="licence-has-input textbox" ng-model="licensed_movers" ng-change="licensed_movers_changed(licensed_movers)">
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>

                <input type="text" name="business-licence-number" placeholder="Enter Licence number" id="licensed_movers_textbox" class="textbox" ng-style="licensed_movers_style" ng-model="business_license_number">

              </div>


              <div class="input-field has-licence-option">
                <P>Do you carry business insurance?</P>
                <small>Do you carry insurance that protects customer's goods? If approved you will be asked to provide a policy
                  number.</small>

                <select name="has_business_insurance" class="licence-has-input textbox"  ng-model="insurance" ng-change="insurance_changed(insurance)">
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>

                <input type="text" name="business-insurance-policy-number" placeholder="Enter insurance policy number" class="textbox" ng-style="insurance_style" ng-model="business_policy_insurance_number">

              </div>

              <div class="input-field has-licence-option">
                <P>Are you a licensed full-service mover?</P>
                <small>This means you own or lease commercial moving trucks and carry the proper state and federal licenses to legally
                  provide intersate (multi-state) and/or intrastate (within your state) moving services.
                </small>

                <select name="is_full-service_mover" class="licence-has-input textbox" ng-model="is_full_service_mover">
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>


              </div>




              <button class="btn btn-orange" type="button" ng-click="SaveLicense(licensed_movers,insurance,is_full_service_mover,business_license_number,business_policy_insurance_number)">Save</button>

          </form>