<div id="wrapper">

    <section id="register-form-container" class="container client-register-form">
      <div class="row center-xs">
        <div class="col-xs-8 form-content">
          <div class="head" id="sign-up-header">
            <h3>Register new client account</h3>
            <p>Do you have already an account ? <a href="client-login.html">Sign in</a></p>
          </div>  
          <form action="#" method="post" id="service-provider-registration">
            <div id="steps-container">
              <div id="steps-progress">
                <div class="progress-bar-container">
                  <div class="progress-bar">
                    <div class="actual-progress"></div>
                  </div>
                </div>
              </div>
              <div id="step-1" class="step">
                <div class="head">
                  <h4>
                    Account information
                  </h4>
                </div>
                <div class="input-field has-icon">
                  <i class="fas fa-user"></i>
                  <label>Username</label>
                  <input type="text" id="username" class="textbox text-box-registration" name="username" maxlength="20"
                    data-validation="custom" data-validation-regexp="^[a-zA-Z_](?![\.-]+$)[a-zA-Z0-9\.-]*$"
                    data-validation-error-msg="Please enter a valid username">
                </div>
                <div class="input-field has-icon">
                  <i class="fas fa-envelope"></i>
                  <label>Email</label>
                  <input type="email" id="email" class="textbox text-box-registration" name="email" data-validation="email" data-validation-error-msg="You did not enter a valid e-mail">
                </div>
                <div class="input-field has-icon">
                  <i class="fas fa-lock"></i>
                  <label>Password</label>
                  <input type="password" id="password" class="textbox text-box-registration no-white-spaces-mask" name="password"
                    data-validation-error-msg="Please enter a strong password" data-validation="strongPassword length"
                    data-validation-length="min8" maxlength="40">
                </div>
                <div class="input-field has-icon">
                  <i class="fas fa-lock"></i>
                  <label>Repeat password</label>
                  <input type="password" id="repeatpassword" class="textbox text-box-registration no-white-spaces-mask"
                    name="r-password" data-validation="repeatPassword length" data-validation-length="min8"
                    data-validation-event="click" maxlength="40">
                </div>
                <button class="step-button next-step" id="NextEmail">Next</button>
              </div>
              <div id="step-2" class="step">
                <div class="head">
                  <h4>
                    Personal information
                  </h4>
                </div>
                <div class="input-row">
                  <div class="input-field half">
                    <label>First name</label>
                    <input type="text" class="textbox text-box-registration" id="firstname" name="firstname" maxlength="20"
                      data-validation-regexp="^([a-zA-Z\s]+)$" data-validation="custom" data-validation-error-msg="Please enter a valid name">
                  </div>
                  <div class="input-field half">
                    <label>Last name</label>
                    <input type="text" class="textbox text-box-registration " id="lastname" name="lastname" maxlength="20"
                      data-validation-regexp="^([a-zA-Z\s]+)$" data-validation="custom" data-validation-error-msg="Please enter a valid name">
                  </div>
                </div>
                <div class="input-row">
                    <div class="input-field half">
                      <label>Primary phone</label>
                      <input type="tel" class="textbox text-box-registration us-phone-number-mask " id="primary_phone"
                        name="primary_phone" placeholder="(XXX) XXX-XXXX" maxlength="14" data-validation="required custom"
                        data-validation-regexp="^[\s\(]((\([0-9]{3}\))|[0-9]{3})[\s\)][\s\ ][\0-9]{3}[\s\-]?[0-9]{4}$">
                    </div>

                    
                    <div class="input-field half">
                        <label>Business address</label>
                        <input type="text" class="textbox text-box-registration " id="business_address" name="business_address"
                          placeholder="Enter your business address" maxlength="20" data-validation="required"
                          data-validation-error-msg="Please enter your business address">
                      </div>




                  </div>

                
                <div class="input-row">
                    <div class="input-field third ">
                      <label>City</label>
                      <input type="text" class="textbox text-box-registration " id="city" name="city" placeholder="Enter your city name"
                        maxlength="20" data-validation="required custom" data-validation-regexp="^([a-zA-Z0-9]+)$"
                        data-validation-error-msg="Please a valid city name">
                    </div>
                    <div class="input-field third ">
                      <select name="state" id="state" data-validation="required">
                        <option value="" selected="selected" disabled>State</option>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="DC">District Of Columbia</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                      </select>
                    </div>
                    <div class="input-field third ">
                      <label>Zip</label>
                      <input type="text" class="textbox text-box-registration integer-number-mask " id="zip" name="zip"
                        placeholder="XXXXX" maxlength="10" data-validation="required">
                    </div>
                  </div>

  
                <button class="step-button next-step">Next</button>
                <button class="step-button previous-step">Previous</button>
              </div>
              <div id="step-3" class="step">
                <div class="head">
                  <h4>
                    Terms and Agreements
                  </h4>
                </div>
                <div class="input-field has-text">
                  <div class="terms">
                    <P>
                      This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean
                      sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id
                      elit.
                      Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam
                      nec
                      tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in
                      elit.

                      Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                      Mauris
                      in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit.
                      Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum
                      feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
                      Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                      Mauris
                      in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit.
                      Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum
                      feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
                      Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                      Mauris
                      in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit.
                      Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum
                      feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
                      This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean
                      sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id
                      elit.
                      Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam
                      nec
                      tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in
                      elit.

                      Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                      Mauris
                      in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit.
                      Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum
                      feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
                      Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                      Mauris
                      in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit.
                      Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum
                      feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
                      Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                      Mauris
                      in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit.
                      Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum
                      feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
                    </P>
                  </div>
                  <div class="input-field has-checkbox">
                    <label>
                      <input id="agree_checked" type="checkbox" data-validation="required">
                      <P> I have read the Helper terms of service and agree to these terms.</P>
                    </label>
                  </div>
                </div>
                <div class="input-field has-text has-textarea">
                  <P>Do you have any messages to us? <span>(Optional)</span></P>
                  <textarea name="optional_message" id="optional_message" class="textbox textarea text-box-registration "
                    placeholder="If yes, Type message here .. "></textarea>
                </div>
                <button class="step-button next-step" id="send-application-btn"><i class="fab fa-telegram-plane"></i> Send Application</button>
                <button class="step-button previous-step">Previous</button>
              </div>
          </form>

          <div id="step-4" class="step">
            <div class="reg-application-sent">
              <div class="head">
                <h4>Send Application</h4>
              </div>
              <h4>
                Thank you for using my moving labor<br>
              </h4>
              <img src="assets/img/new-client-registered.jpg" alt="Done !">
              <p>
                Please check your email address , We’ve sent you an account confirmation link
              </p>
              <button class="btn btn-orange" id="resend-regsiter-email">Resend email <i class="fas fa-envelope"></i></button>
            </div>
          </div>
        </div>
      </div>
  </div>