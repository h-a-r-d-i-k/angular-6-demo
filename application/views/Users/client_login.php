<div id="wrapper">

    <section id="login-form-container" class="container">
      <div class="row center-xs middle-xs">

          <div class="col-xs-5 sign-in-banner">
            <div class="img-container">
                <div>
                    <h4>Get a help today</h4>
                    <p>No more moving pain and wasting time <br>
                        My Moving Labor makes moving easier !</p>
                  </div>
            </div>

          </div>

        <div class="col-xs-4 sign-in-form-content">

          <div class="head" id="sign-up-header">
            <h3>Clients area</h3>
            <p>Need to hire a helper ? <br>
              <a href="client-profile-register.html">Create your account  now </a></p>
          </div>  
          <form action="#" method="post">

     
           
              <div class="input-field has-icon">
                  <i class="fas fa-user"></i>
                  <label>Username / Email</label>
                  <input type="text" id="username" class="textbox text-box-registration" name="username" maxlength="20">
                </div>
  
                <div class="input-field has-icon">
                    <i class="fas fa-lock"></i>
                    <label>Password</label>
                    <input type="password" id="password" class="textbox text-box-registration no-white-spaces-mask" name="password"maxlength="40">
                  </div>

                  <button type="submit" class="btn btn-orange">SIGN IN</button>
                  <a href="reset-password.html">Forgot password ?</a>
          </form>
        </div>
      </div>
  </div>