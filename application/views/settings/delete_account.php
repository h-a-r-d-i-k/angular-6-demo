
          <div class="row head">
            <h4>Delete your account permanently</h4>
          </div>

          <form action="#" method="get">
            <div class="col-sm-8 input-field delete-account">
              <p><i class="fas fa-exclamation-triangle"></i> Be careful, <br>Once you delete your account, there is no
                going back. Please be certain.</p>

              <div class="input-field inline-input ">
                <h5>Current password</h5>
                <input type="password" class="textbox " ng-model="password" name="password">
              </div>
              <button class="btn btn-orange" type="button" ng-click="deleteAccount(password)">Delete account</button>
          </form>

        