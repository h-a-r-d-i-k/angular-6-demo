
          <div class="row head">
            <h4>Payment settings</h4>
          </div>

          <div class="get-paid">
            <form action="#" method="get">
              <div class="input-field ">
                <p>Your Balance is : <span class="bold-700">$560</span></p>
                <button class="btn btn-dark-blue btn-get-paid" id="get-paid"><i class="fas fa-credit-card"></i> Get
                  paid
                  now</button>
              </div>
            </form>
            <div class="last-payment">
              <div class="row head">
                <h4>Payment details</h4>
              </div>
              <p class="bold-700">Last Payment :</p>
              <p class="sub">$700 on Sep 5, 2018 to PayPal account</p>
            </div>
            <form action="#" method="get">
              <div class="row head">
                <h4>Payment Methods</h4>
              </div>
              <div class="input-field">
                <p>Connect your PayPal</p>
                <button class="btn btn-paypal"><i class="fab fa-paypal"></i> Connect PayPal</button>
              </div>
            </form>
          </div>

