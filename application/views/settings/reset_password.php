
          <div class="row head">
            <h4>Update password</h4>
          </div>

          <!-- Change property starts-->
          <form action="#" method="get">

            <div class="col-sm-8">
              <div class="input-field inline-input ">
                <h5>Old password</h5>
                <input type="password" class="textbox changepassword" ng-model="old_password" name="old-password" maxlength="25">
              </div>
              <div class="input-field inline-input ">
                <h5>Your new password</h5>
                <input type="password" class="textbox changepassword" ng-model="new_password" name="new-password" maxlength="25">
              </div>
              <div class="input-field inline-input ">
                <h5>Confirm new password</h5>
                <input type="password" class="textbox changepassword" ng-model="confirm_password" name="confirm-password" maxlength="25">
              </div>
            </div>
            <button class="btn btn-orange" type="button" ng-click="changePassword(old_password,new_password,confirm_password)">Update password</button>
          </form>
