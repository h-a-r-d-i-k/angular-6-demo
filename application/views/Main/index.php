  <div id="wrapper">

    <section id="intro" class="container center responsive-top-margin responsive-bottom-padding with-bottom-separator">
      <div class="row center-xs">

        <div class="col-md-6">
          <h2 class="bold-700">We move stuff for you</h2>
          <h5 class="sub">Search , Compare, Book and Relax</h5>
          <a href="#" class="btn btn-orange btn-large preventDefault" id="concept-video-popup">
            <i class="fas fa-play"></i> See concept video
          </a>
          <p class="sub">We let you choose from +2500 Movers all over the country <br> Get the best helper today</p>

        </div>
        <div class="col-md-6">
          <img src="<?php echo base_url(); ?>assets/img/delivery-man-450.jpg" alt="delivery-man">
        </div>
      </div>

    </section>


    <section id="how-it-works-section" class="container center responsive-top-margin responsive-bottom-padding with-bottom-separator">
      <div class="row">
        <h2 class="bold-700">How it works ?</h2>
        <div class="col-sm-3">
          <img src="<?php echo base_url(); ?>assets/img/Search.png" alt="search">
          <h4 class="bold-700">Search</h4>
          <p class="sub">Search of a local helper<br>by zip code</p>
        </div>
        <div class="col-sm-3">
          <img src="<?php echo base_url(); ?>assets/img/Hire.png" alt="Compare">
          <h4 class="bold-700">Compare</h4>
          <p class="sub">Compare helper<br>with different rates</p>
        </div>
        <div class="col-sm-3">
          <img src="<?php echo base_url(); ?>assets/img/Move.png" alt="Book">
          <h4 class="bold-700">Book </h4>
          <p class="sub">Meet them<br>Start moving your stuff</p>
        </div>
        <div class="col-sm-3">
          <img src="<?php echo base_url(); ?>assets/img/Done.png" alt="Relax">
          <h4 class="bold-700">Relax</h4>
          <p class="sub">Get moved<br>Approve the payment</p>
        </div>
      </div>
    </section>



    <section id="customers-reviews" class="container center responsive-top-margin responsive-bottom-padding with-bottom-separator">
      <div class="row head">
        <h2 class="bold-700">Recent Customer Reviews</h2>
        <p class="sub">The average service provider rating is 4.7 stars out of 5.0 after +25K customer reviews.</p>
      </div>


      <div id="testimonials-slider" class="row">
        <div class="single-testimonial">
          <div class="review-body">
            <h5>Reviewing : Convenient Lifestyles Moving Inc</h5>
            <p><i class="fas fa-quote-left"></i>
              They unloaded a pod. They were fast and friendly. They were on time and we're contientious
              of how/where we wanted things. I would reommend them ... <a href="#"> Read more</a>
            </p>
            <?xml version="1.0" encoding="utf-8"?>
            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <svg version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
              x="0px" y="0px" width="20px" height="19.56px" viewBox="0 0 30 19.56" xml:space="preserve">
              <polygon fill="#4674c8" points="0,0 15,19.56 30,0.243 30.284,-0.042 " />
            </svg>

          </div>
          <div class="review-author">
            <img class="review-author-image" src="<?php echo base_url(); ?>assets/img/user-img-2.png" alt="client img">
            <h4>Jessy CORR</h4>
            <p>Pompano Beach FL - <span class="order-date">6/7/2018</span></p>
            <div class="stars-bar 5-stars"><img src="<?php echo base_url(); ?>assets/img/stars-bar.png" alt=" "></div>
          </div>
        </div>

        <div class="single-testimonial">
          <div class="review-body">
            <h5>Reviewing : Convenient Lifestyles Moving Inc</h5>
            <p><i class="fas fa-quote-left"></i>
              They unloaded a pod. They were fast and friendly. They were on time and we're contientious
              of how/where we wanted things. I would reommend them ... <a href="#"> Read more</a>
            </p>
            <?xml version="1.0" encoding="utf-8"?>
            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <svg version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
              x="0px" y="0px" width="20px" height="19.56px" viewBox="0 0 30 19.56" xml:space="preserve">
              <polygon fill="#4674c8" points="0,0 15,19.56 30,0.243 30.284,-0.042 " />
            </svg>

          </div>
          <div class="review-author">
            <img class="review-author-image" src="<?php echo base_url(); ?>assets/img/user-img-2.png" alt="client img">
            <h4>Jessy CORR</h4>
            <p>Pompano Beach FL - <span class="order-date">6/7/2018</span></p>
            <div class="stars-bar 5-stars"><img src="<?php echo base_url(); ?>assets/img/stars-bar.png" alt=" "></div>
          </div>
        </div>

        <div class="single-testimonial">
          <div class="review-body">
            <h5>Reviewing : Convenient Lifestyles Moving Inc</h5>
            <p><i class="fas fa-quote-left"></i>
              They unloaded a pod. They were fast and friendly. They were on time and we're contientious
              of how/where we wanted things. I would reommend them ... <a href="#"> Read more</a>
            </p>

            <?xml version="1.0" encoding="utf-8"?>
            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <svg version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
              x="0px" y="0px" width="20px" height="19.56px" viewBox="0 0 30 19.56" xml:space="preserve">
              <polygon fill="#4674c8" points="0,0 15,19.56 30,0.243 30.284,-0.042 " />
            </svg>

          </div>
          <div class="review-author">
            <img class="review-author-image" src="<?php echo base_url(); ?>assets/img/user-img-2.png" alt="client img">
            <h4>Jessy CORR</h4>
            <p>Pompano Beach FL - <span class="order-date">6/7/2018</span></p>
            <div class="stars-bar 5-stars"><img src="<?php echo base_url(); ?>assets/img/stars-bar.png" alt=" "></div>
          </div>
        </div>


        <div class="single-testimonial">
          <div class="review-body">
            <h5>Reviewing : Convenient Lifestyles Moving Inc</h5>
            <p><i class="fas fa-quote-left"></i>
              They unloaded a pod. They were fast and friendly. They were on time and we're contientious
              of how/where we wanted things. I would reommend them ... <a href="#"> Read more</a>
            </p>

            <?xml version="1.0" encoding="utf-8"?>
            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <svg version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
              x="0px" y="0px" width="20px" height="19.56px" viewBox="0 0 30 19.56" xml:space="preserve">
              <polygon fill="#4674c8" points="0,0 15,19.56 30,0.243 30.284,-0.042 " />
            </svg>

          </div>
          <div class="review-author">
            <img class="review-author-image" src="<?php echo base_url(); ?>assets/img/user-img-2.png" alt="client img">
            <h4>Jessy CORR</h4>
            <p>Pompano Beach FL - <span class="order-date">6/7/2018</span></p>
            <div class="stars-bar 5-stars"><img src="<?php echo base_url(); ?>assets/img/stars-bar.png" alt=" "></div>
          </div>
        </div>


        <div class="single-testimonial">
          <div class="review-body">
            <h5>Reviewing : Convenient Lifestyles Moving Inc</h5>
            <p><i class="fas fa-quote-left"></i>
              They unloaded a pod. They were fast and friendly. They were on time and we're contientious
              of how/where we wanted things. I would reommend them ... <a href="#"> Read more</a>
            </p>

            <?xml version="1.0" encoding="utf-8"?>
            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <svg version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
              x="0px" y="0px" width="20px" height="19.56px" viewBox="0 0 30 19.56" xml:space="preserve">
              <polygon fill="#4674c8" points="0,0 15,19.56 30,0.243 30.284,-0.042 " />
            </svg>

          </div>
          <div class="review-author">
            <img class="review-author-image" src="<?php echo base_url(); ?>assets/img/user-img-2.png" alt="client img">
            <h4>Jessy CORR</h4>

            <p>Pompano Beach FL - <span class="order-date">6/7/2018</span></p>
            <div class="stars-bar 5-stars"><img src="<?php echo base_url(); ?>assets/img/stars-bar.png" alt=" "></div>
          </div>
        </div>

      </div>
    </section>


    <section id="helpers-slider-section" class="container-fluid center responsive-top-margin responsive-bottom-padding with-bottom-separator">
      <div class="row head">
        <h2 class="bold-700">Choose and compare helpers</h2>
        <p class="sub">Among +2500 top rated service providers</p>
      </div>

      <div id="service-provider-slider" class="row">
        <div class="service-provider col-sm-3">
          <div class="sp-img"><img src="<?php echo base_url(); ?>assets/img/user-img.png" alt="sp-name"></div>
          <div class="sp-info">
            <h5>Perfect choice movers</h5>
            <p>New york,2223</p>
            <a href="#" class="">Book</a> <a href="#" class="contact">Contact</a>
          </div>
          <div class="sp-footer">
            <p class="hires"><span>68</span> Hires,</p>
            <p class="status">Top rated</p>
            <div class="rating">
              <p>5/5</p><i class="fas fa-star"></i>
            </div>
          </div>
        </div>

        <div class="service-provider col-sm-3">
          <div class="sp-img"><img src="<?php echo base_url(); ?>assets/img/user-img.png" alt="sp-name"></div>
          <div class="sp-info">
            <h5>Perfect choice movers</h5>
            <p>New york,2223</p>
            <a href="#" class="">Book</a> <a href="#" class="contact">Contact</a>
          </div>
          <div class="sp-footer">
            <p class="hires"><span>68</span> Hires,</p>
            <p class="status">Top rated</p>
            <div class="rating">
              <p>5/5</p><i class="fas fa-star"></i>
            </div>
          </div>
        </div>


        <div class="service-provider col-sm-3">
          <div class="sp-img"><img src="<?php echo base_url(); ?>assets/img/user-img.png" alt="sp-name"></div>
          <div class="sp-info">
            <h5>Perfect choice movers</h5>
            <p>New york,2223</p>
            <a href="#" class="">Book</a> <a href="#" class="contact">Contact</a>
          </div>
          <div class="sp-footer">
            <p class="hires"><span>68</span> Hires,</p>
            <p class="status">Top rated</p>
            <div class="rating">
              <p>5/5</p><i class="fas fa-star"></i>
            </div>
          </div>
        </div>

        <div class="service-provider col-sm-3">
          <div class="sp-img"><img src="<?php echo base_url(); ?>assets/img/user-img.png" alt="sp-name"></div>
          <div class="sp-info">
            <h5>Perfect choice movers</h5>
            <p>New york,2223</p>
            <a href="#" class="">Book</a> <a href="#" class="contact">Contact</a>
          </div>
          <div class="sp-footer">
            <p class="hires"><span>68</span> Hires,</p>
            <p class="status">Top rated</p>
            <div class="rating">
              <p>5/5</p><i class="fas fa-star"></i>
            </div>
          </div>
        </div>



        <div class="service-provider col-sm-3">
          <div class="sp-img"><img src="<?php echo base_url(); ?>assets/img/user-img.png" alt="sp-name"></div>
          <div class="sp-info">
            <h5>Perfect choice movers</h5>
            <p>New york,2223</p>
            <a href="#" class="">Book</a> <a href="#" class="contact">Contact</a>
          </div>
          <div class="sp-footer">
            <p class="hires"><span>68</span> Hires,</p>
            <p class="status">Top rated</p>
            <div class="rating">
              <p>5/5</p><i class="fas fa-star"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="row"><a href="#" class="btn btn-white">Load more helpers</a></div>
    </section>

    <section id="get-app-section" class="container center responsive-top-margin responsive-bottom-padding with-bottom-separator">
      <div class="row center-xs">
        <div class="col-md-6 col-xs-12 img-cont"><img src="<?php echo base_url(); ?>assets/img/get-the-app.jpg" alt="Get the app-links"></div>
        <div class="col-md-6 col-xs-12 body-cont">
          <h2 class="bold-700">Get helped any time, any Place</h2>
          <p class="sub">Download our Mobile App, Reach your helpers easier</p>
          <div class="btn-group"><a href="#" class="btn btn-orange"><i class="fab fa-apple"></i> App Store</a><a href="#"
              class="btn btn-white"><i class="fab fa-android"></i> Google Play</a></div>
        </div>
      </div>
    </section>

    <section id="why-choose-us-section" class="container center responsive-top-margin responsive-bottom-padding with-bottom-separator">
      <div class="row head">
        <h2 class="bold-700">Why to use My Moving Labor ?</h2>
      </div>
      <div class="row">
        <div class="col-sm-3 col-xs-12">
          <img src="<?php echo base_url(); ?>assets/img/cash.png" alt="cash">
          <h4 class="bold-600">Fixed, Up-front Pricing</h4>
          <p class="sub">The price you see is the price you pay, No extra fees or charges</p>
        </div>
        <div class="col-sm-3 col-xs-12">
          <img src="<?php echo base_url(); ?>assets/img/heart.png" alt="heart">
          <h4 class="bold-600">Real customer reviews
          </h4>
          <p class="sub">Search for helpers, Hire the highest quality</p>
        </div>
        <div class="col-sm-3 col-xs-12">
          <img src="<?php echo base_url(); ?>assets/img/gaurd.png" alt="guard">
          <h4 class="bold-600">Service is Guarnteed</h4>
          <p class="sub">Complimentary Standard Repair Coverage, Upto $1,000 covergae on damges</p>
        </div>
        <div class="col-sm-3 col-xs-12">
          <img src="<?php echo base_url(); ?>assets/img/help.png" alt="help">
          <h4 class="bold-600">Customer Support</h4>
          <p class="sub">Ready to help you 24 / 7 / 356, We are happy to get your issue solved</p>
        </div>
      </div>
    </section>



    <section id="fqa-home-section" class="container responsive-top-margin responsive-bottom-padding with-bottom-separator">
      <div class="row accordion">
        <ul>
          <li>
            <input type="checkbox" checked>
            <i></i>
            <h2>Who are the helpers ?</h2>
            <p>The move Helpers you see on MyMovingLabor do loading and unloading jobs for a living
              <br>
              Many are professional labor-only moving companies that specialize in load and unload services. Others are
              full-service moving companies willing to send their workers out on labor-only moving jobs</p>
          </li>
          <li>
            <input type="checkbox" checked>
            <i></i>
            <h2>What happens if they break my stuff ?</h2>
            <p>Using the sibling and checked selectors, we can determine the styling of sibling elements based on the
              checked state of the checkbox input element. One use, as demonstrated here, is an entirely CSS and HTML
              accordion element. Media queries are used to make the element responsive to different screen sizes.</p>
          </li>
          <li>
            <input type="checkbox" checked>
            <i></i>
            <h2>Who is My Moving Labor ?</h2>
            <p>By making the open state default for when :checked isn't detected, we can make this system accessable
              for browsers that don't recognize :checked. The fallback is simply an open accordion. The accordion can
              be manipulated with Javascript (if needed) by changing the "checked" property of the input element.</p>
          </li>
        </ul>
      </div>
    </section>



    <section id="partners-section" class="container center responsive-top-margin responsive-bottom-padding ">
      <div class="row head">
        <h2 class="bold-700">My Moving labor is featured on</h2>
      </div>
      <div class="row center-xs">
        <div class="fixed-basis"><img src="<?php echo base_url(); ?>assets/img/abc-11-news.png" alt="abc-11-news"></div>
        <div class="fixed-basis"><img src="<?php echo base_url(); ?>assets/img/foxnews.png" alt="foxnews"></div>
        <div class="fixed-basis"><img src="<?php echo base_url(); ?>assets/img/inc.png" alt="inc"></div>
        <div class="fixed-basis"><img src="<?php echo base_url(); ?>assets/img/kake.png" alt="kake"></div>
        <div class="fixed-basis"><img src="<?php echo base_url(); ?>assets/img/lifehacker.png" alt="lifehacker"></div>
        <div class="fixed-basis"><img src="<?php echo base_url(); ?>assets/img/mashable.png" alt="mashable"></div>
      </div>
    </section>
    <section id="available-states-section" class="">
      <div class="row head">
        <h2 class="bold-600">My moving labor has movers nation wide</h2>
      </div>
      <div class="row center-xs">
        <div class="col-xs-4">
          <ul>
            <li>NEW YORK<span>31</span></li>
            <li>CHICAGO<span>10</span></li>
            <li>HOUSTON<span>87</span></li>
            <li>SAN DIEGO<span>12</span></li>
            <li>COLORADO SPRINGS<span>3</span></li>
            <li>PORTLAND<span>35</span></li>
          </ul>
        </div>
        <div class="col-xs-4">
          <ul>
            <li>TUCSON<span>12</span></li>
            <li>FORT WORTH<span>21</span></li>
            <li>ORLANDO<span>8</span></li>
            <li>SPOKANE<span>7</span></li>
            <li>ATLANTA<span>12</span></li>
            <li>LAS VEGAS<span>9</span></li>
            <li>TAMPA<span>36</span></li>
          </ul>
        </div>
        <div class="col-xs-4">
          <ul>
            <li>DENVER<span>34</span></li>
            <li>CHARLOTTE<span>52</span></li>
            <li>LOS ANGELES<span>40</span></li>
            <li>SAN ANTONIO<span>5</span></li>
            <li>WASHINGTON<span>19</span></li>
            <li>COLUMBUS<span>12</span></li>
            <li>DALLAS<span>13</span></li>
          </ul>



        </div>
      </div>
    </section>
  </div>
