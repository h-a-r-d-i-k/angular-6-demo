<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('sendMail'))
{
	function sendMail($data,$message)
	{
		$CI =& get_instance();

		
		$CI->load->library('email');
		$config['protocol'] 	= $CI->config->item('protocol');
		$config['smtp_host'] 	= $CI->config->item('smtp_host');
		$config['smtp_port'] 	= $CI->config->item('smtp_port');
		$config['smtp_user'] 	= $CI->config->item('smtp_user');
		$config['smtp_pass'] 	= $CI->config->item('smtp_pass');
		$config['charset'] 		= $CI->config->item('charset');
		$config['newline'] 		= $CI->config->item('newline');
		$config['mailtype'] 	= $CI->config->item('mailtype');

		// print_r($config);exit;
		// $config['crlf'] 	= $CI->config->item('crlf');
		// $config['wordwrap'] 	= $CI->config->item('wordwrap');
		$CI->email->initialize($config);
		$CI->email->from($config['smtp_user'], $CI->config->item('project_name'));
		$CI->email->to($data['to_email']);

		$message = $message;


		$CI->email->subject($data['subject']);
		$CI->email->message($message);
		if($CI->email->send()){
			$response=array("success"=>"1","message"=>"Mail Sent Successfully");
		}
		else{
			$response=array("success"=>"0","message"=>$CI->email->print_debugger());
			//$response=array("success"=>"0","message"=>"Email sending failure, please try again!");
		}
		return $response;
	}
}


